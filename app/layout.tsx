import type { Metadata } from 'next';
import localFont from 'next/font/local';
import './globals.css';
import ReduxProvider from '@/components/providers/ReduxProvider';

const GSans = localFont({
  src: [
    {
      path: 'fonts/general-sans/GeneralSans-Extralight.woff2',
      weight: '200',
    },
    {
      path: 'fonts/general-sans/GeneralSans-Light.woff2',
      weight: '300',
    },
    {
      path: 'fonts/general-sans/GeneralSans-Regular.woff2',
      weight: '400',
    },
    {
      path: 'fonts/general-sans/GeneralSans-Medium.woff2',
      weight: '500',
    },
    {
      path: 'fonts/general-sans/GeneralSans-Semibold.woff2',
      weight: '600',
    },
    {
      path: 'fonts/general-sans/GeneralSans-Bold.woff2',
      weight: '700',
    },
  ],
  variable: '--font-general-sans',
});

export const metadata: Metadata = {
  title: 'Oxinos - Base Frontend',
  description: 'This is a base for Oxinos projects',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${GSans.className}`}>
        <ReduxProvider>
          <div className="w-[1281px]:hidden hidden md:block">{children}</div>
        </ReduxProvider>
      </body>
    </html>
  );
}
