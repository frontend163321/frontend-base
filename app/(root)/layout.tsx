import dynamic from 'next/dynamic';

const CoreModal = dynamic(() => import('@/components/modal/CoreModal'), {
  ssr: false,
});

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <main className="font-inter flex h-screen w-full">
      {children}
      <CoreModal />
    </main>
  );
}
