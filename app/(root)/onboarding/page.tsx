'use client';

import DotsIndicator from '@/components/onboarding/DotsIndicator';
import SliderContent from '@/components/onboarding/SliderContent';
import Hero from '@/components/shared/Hero';
import { Button } from '@/components/ui/button';
import { CarouselApi, CarouselItem } from '@/components/ui/carousel';
import { onBoarding } from '@/constants';
import styles from '@styles/modules/onboarding.module.css';
import { setCookie } from 'cookies-next';
import { useState } from 'react';

const OnBoardingPage = () => {
  const [api, setApi] = useState<CarouselApi>();
  const [current, setCurrent] = useState<number>(0);
  const [count, setCount] = useState<number>(0);

  const handleCurrent = (params: string) => {
    if (api) {
      params === 'next'
        ? api?.scrollTo(current)
        : setCookie('onboarding', false);
    }
  };

  return (
    <div className={styles.onboarding}>
      <SliderContent
        api={api}
        setApi={setApi}
        setCurrent={setCurrent}
        setCount={setCount}
      >
        {onBoarding?.map((item, index) => (
          <CarouselItem
            className="flex w-full flex-col items-center justify-center"
            key={index}
          >
            <Hero
              src={item.image}
              heading={item.heading}
              description={item.description}
            />
          </CarouselItem>
        ))}
      </SliderContent>

      <DotsIndicator count={count} current={current} />

      <div className={styles.buttonArea}>
        <Button
          className={`${styles.content} w-full`}
          onClick={() => handleCurrent('next')}
        >
          {current === count ? 'Selesai' : 'Selanjutnya'}
        </Button>

        {current !== count && (
          <div
            className={`${styles.skip} absolute bottom-[-40px]`}
            onClick={() => handleCurrent('skip')}
          >
            Lewati
          </div>
        )}
      </div>
    </div>
  );
};

export default OnBoardingPage;
