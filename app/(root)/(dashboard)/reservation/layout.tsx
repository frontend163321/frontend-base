import HeaderReservation from '@/components/header/HeaderReservation';
import React from 'react';

const layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className="w-full max-h-screen overflow-hidden">
      <HeaderReservation />

      {children}
    </div>
  );
};

export default layout;
