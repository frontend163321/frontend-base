import MainContent from '@/components/dashboard-reservation/MainContent';
import LayoutMain from '@/components/layout/LayoutMain';
import MenuBar from '@/components/shared/MenuBar';

import { reservationTabMenu } from '@/constants';

const ReservationPage = ({ params }: { params: { slug: string } }) => {
  return (
    <LayoutMain>
      <MenuBar data={reservationTabMenu} active={params.slug} />

      <MainContent />
    </LayoutMain>
  );
};

export default ReservationPage;
