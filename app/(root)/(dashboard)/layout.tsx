'use client';

import NavigationHeader from '@/components/shared/NavigationHeader';
import { navigationHeader } from '@/lib/utils';
import { usePathname } from 'next/navigation';
import React from 'react';

const layout = ({
  children,
  params,
}: {
  children: React.ReactNode;
  params: { slug: string };
}) => {
  const pathname = usePathname();
  const navigation = navigationHeader(pathname);

  return (
    <div className="w-full max-h-screen overflow-hidden">
      {navigation?.path && (
        <NavigationHeader title={navigation.Label} path={navigation.path} />
      )}
      {children}
    </div>
  );
};

export default layout;
