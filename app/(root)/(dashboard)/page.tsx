import AsideContent from '@/components/dashboard-cashier/AsideContent';
import MainContent from '@/components/dashboard-cashier/MainContent';
import LayoutAside from '@/components/layout/LayoutAside';
import LayoutContent from '@/components/layout/LayoutContent';
import styles from '@styles/modules/dashboard.module.css';

const DashboardPage = () => {
  return (
    <div className={styles.dashboard}>
      <LayoutContent>
        <MainContent />
      </LayoutContent>

      <LayoutAside>
        <AsideContent />
      </LayoutAside>
    </div>
  );
};

export default DashboardPage;
