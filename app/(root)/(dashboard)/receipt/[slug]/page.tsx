import MainContentAside from '@/components/dashboard-receipt/MainContentAside';
import MainContentDetail from '@/components/dashboard-receipt/MainContentDetail';
import LayoutAside from '@/components/layout/LayoutAside';
import LayoutContent from '@/components/layout/LayoutContent';
import styles from '@styles/modules/dashboard.module.css';

const DetailReceiptPage = () => {
  return (
    <div className={styles.dashboard}>
      <LayoutContent>
        <MainContentDetail />
      </LayoutContent>

      <LayoutAside shadow={false}>
        <MainContentAside />
      </LayoutAside>
    </div>
  );
};

export default DetailReceiptPage;
