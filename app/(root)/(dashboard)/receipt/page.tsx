import MainContent from '@/components/dashboard-receipt/MainContent';
import HeaderReceipt from '@/components/header/HeaderReceipt';

const ReceiptPage = () => {
  return (
    <>
      <HeaderReceipt />
      <MainContent />
    </>
  );
};

export default ReceiptPage;
