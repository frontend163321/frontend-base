import AsideListOrderContent from '@/components/dashboard-list-order/AsideListOrderContent';
import MainContent from '@/components/dashboard-list-order/MainContent';
import LayoutAside from '@/components/layout/LayoutAside';
import LayoutContent from '@/components/layout/LayoutContent';
import { cn } from '@/lib/utils';
import styles from '@styles/modules/dashboard.module.css';

const ListOrderPage = () => {
  return (
    <div className={cn(styles.dashboard)}>
      <LayoutAside shadow="right">
        <AsideListOrderContent />
      </LayoutAside>

      <LayoutContent className="relative">
        <MainContent />
      </LayoutContent>
    </div>
  );
};

export default ListOrderPage;
