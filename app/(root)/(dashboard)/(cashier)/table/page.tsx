'use client';

import CardTable from '@/components/cards/CardTable';
import BottomSheet from '@/components/dashboard-cashier/BottomSheet';
import HeaderFilterTable from '@/components/dashboard-cashier/HeaderFilterTable';
import LayoutMain from '@/components/layout/LayoutMain';
import { useState } from 'react';

const DashboardTable = () => {
  const [active, setActive] = useState<string>('Semua');
  const [selectedTable, setSelectedTable] = useState<string[]>([]);

  return (
    <div className="relative">
      <LayoutMain className="pe-0 pb-0">
        <HeaderFilterTable active={active} setActive={setActive} />
      </LayoutMain>

      <LayoutMain className="pt-0">
        <div className="grid grid-cols-5 gap-3 mt-4 h-svh overflow-y-auto pb-44">
          {Array.from({ length: 50 }).map((_, index) => (
            <CardTable
              selectedTable={selectedTable}
              setSelectedTable={setSelectedTable}
              type={active}
            />
          ))}
        </div>
      </LayoutMain>

      <BottomSheet />
    </div>
  );
};

export default DashboardTable;
