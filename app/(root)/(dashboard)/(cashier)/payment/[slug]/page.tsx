import AsidePaymentContent from '@/components/dashboard-payment/AsideContent';
import BottomSheet from '@/components/dashboard-payment/BottomSheet';
import WrapMainContent from '@/components/dashboard-payment/WrapMainContent';
import LayoutAside from '@/components/layout/LayoutAside';
import LayoutContent from '@/components/layout/LayoutContent';

import { cn } from '@/lib/utils';
import styles from '@styles/modules/dashboard.module.css';

const PaymentCashPage = ({ params }: { params: { slug: string } }) => {
  return (
    <div className={cn(styles.dashboard)}>
      <LayoutAside shadow="right">
        <AsidePaymentContent />
      </LayoutAside>

      <LayoutContent className="relative">
        <WrapMainContent slug={params.slug} />

        <BottomSheet />
      </LayoutContent>
    </div>
  );
};

export default PaymentCashPage;
