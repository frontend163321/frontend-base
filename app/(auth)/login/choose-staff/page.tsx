import FormLoginStaff from '@/components/forms/FormLoginStaff';
import AsideContent from '@/components/auth/AsideContent';
import MainContent from '@/components/auth/MainContent';
import Hero from '@/components/shared/Hero';
import { HeroLoginStaff } from '@/lib/images';

const LoginStaffPage = () => {
  return (
    <>
      <MainContent brandLogo>
        <Hero
          src={HeroLoginStaff}
          heading="Buka Pintu Kesuksesan Restoran Anda!"
          description="Langkah Awal Menuju Pengelolaan Lebih Efisien."
          wrapText
        />
      </MainContent>

      <AsideContent>
        <h1 className="text-heading-2 mb-[40px]">Masuk Pengkasiran</h1>
        <FormLoginStaff />
      </AsideContent>
    </>
  );
};

export default LoginStaffPage;
