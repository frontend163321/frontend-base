import FormLoginMerchant from '@/components/forms/FormLoginMerchant';
import AsideContent from '@/components/auth/AsideContent';
import MainContent from '@/components/auth/MainContent';
import Hero from '@/components/shared/Hero';
import { HeroLoginMerchant, OrderiaLogo } from '@/lib/images';
import styles from '@styles/modules/auth.module.css';
import Image from 'next/image';

const LoginMerchantPage = () => {
  return (
    <>
      <MainContent>
        <Hero
          src={HeroLoginMerchant}
          heading="Buka Pintu Kesuksesan Restoran Anda!"
          description="Langkah Awal Menuju Pengelolaan Lebih Efisien."
          wrapText
        />
      </MainContent>

      <AsideContent>
        <div className={styles.logo}>
          <Image src={OrderiaLogo} alt="Orderia Logo" />
        </div>

        <h1 className="text-heading-2 mb-3">Login</h1>
        <p className="text-body-large">
          Login dengan memasukkan No. Hp yang sudah terdaftar di aplikasi
          Merchant.
        </p>

        <div className="mt-[40px]">
          <FormLoginMerchant />
        </div>
      </AsideContent>
    </>
  );
};

export default LoginMerchantPage;
