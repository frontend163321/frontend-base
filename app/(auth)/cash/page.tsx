import AsideContent from '@/components/auth/AsideContent';
import MainContent from '@/components/auth/MainContent';
import Profile from '@/components/cash/Profile';
import FormEnterCash from '@/components/forms/FormEnterCash';

const EnterCashPage = () => {
  return (
    <>
      <MainContent>
        <Profile />
      </MainContent>

      <AsideContent>
        <h1 className="text-heading-2 mb-[40px]">Input Kas Awal</h1>
        <FormEnterCash />
      </AsideContent>
    </>
  );
};

export default EnterCashPage;
