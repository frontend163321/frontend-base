import * as z from 'zod';
import { phoneNumberRegex } from './regex';

export const AboutSchema = z.object({
  name: z.string().min(5).max(50),
  email: z.string().email(),
});

export const LoginMerchantSchema = z.object({
  phoneNumber: z
    .string()
    .min(10, phoneNumberRegex.msg.min)
    .max(13, phoneNumberRegex.msg.max),
});
