import { routeNavigationHeader } from '@/constants';
import { clsx, type ClassValue } from 'clsx';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const roundedNumpad = (index: number) => {
  switch (index + 1) {
    case 1:
      return 'rounded-tl-base';
    case 3:
      return 'rounded-tr-base';
    case 10:
      return 'rounded-bl-base';
    case 12:
      return 'rounded-br-base';
    default:
      return '';
  }
};

export const borderNumpad = (index: number) => {
  const isBottomRow = index >= 12;
  const isRightColumn = (index + 1) % 13 === 0;

  let classes = 'border-neutral-300';

  if (!isBottomRow) {
    classes += ' border-b';
  }
  if (!isRightColumn) {
    classes += ' border-r';
  }

  if (index < 3) {
    classes += ' border-t';
  }
  if (index % 3 === 0) {
    classes += ' border-l';
  }

  return classes;
};

export const inputSize = (size: Size) => {
  const _size = size === 'big' ? 'h-[48px]' : 'h-[40px]';
  return _size;
};

export const navigationHeader = (pathName: string) => {
  const slug = pathName.split('/')[1];

  if (slug) {
    return routeNavigationHeader.filter(item => item.path === `/${slug}`)?.[0];
  }

  return routeNavigationHeader.filter(item => item.path === pathName)?.[0];
};
