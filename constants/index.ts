import {
  BookIcon,
  CasherIcon,
  CloseCasherIcon,
  HeroOnboarding1,
  HeroOnboarding2,
  HeroOnboarding3,
  NotificationIcon,
  PresentationIcon,
  ReceiptIcon,
  ReserveIcon,
  SettingIcon,
  TableIcon,
} from '@/lib/images';

export const rightHeader = [
  {
    label: 'Daftar Pesanan',
    icon: BookIcon,
  },
  {
    label: 'Meja',
    icon: TableIcon,
  },
  {
    label: '',
    icon: NotificationIcon,
  },
];

// On-boarding
export const onBoarding = [
  {
    image: HeroOnboarding1,
    heading: 'Fokus pada Layanan Pelanggan Luar Biasa!',
    description:
      'Manfaatkan Manajemen Pesanan yang Efisien dan Pembayaran yang Beragam untuk Menghadirkan Pengalaman Terbaik bagi Pelanggan Anda.',
  },
  {
    image: HeroOnboarding2,
    heading: 'Selalu Berada di Depan Tren Bisnis!',
    description:
      'Jadilah yang Pertama Mengetahui Peluang Bisnis Anda dengan Laporan Analitik Terperinci yang Disediakan oleh Orderia.',
  },
  {
    image: HeroOnboarding3,
    heading: 'Optimalisasi Kapasitas Meja, Kurangi Waktu Tunggu!',
    description:
      "Dengan Orderia, Tidak Ada Lagi 'Kehabisan Meja'. Kami Akan Membantu Anda Mengoptimalkan Kapasitas Duduk dan Mengurangi Waktu Tunggu Pelanggan.",
  },
];

// login
export const selectData = [
  {
    value: 'merchant',
    label: 'Merchant',
  },
  {
    value: 'staff',
    label: 'Staff',
  },
];

// Filter Dashboard
export const filterDashboard = [
  'Semua',
  'Hidangan Pembuka',
  'Hidangan Utama',
  'Hidangan Penutup',
  'Minuman',
  'Desert',
  'Es Krim',
];

// reseravtion type
export const reservationType = [
  { value: 'dine-in', label: 'Dine In' },
  { value: 'take-away', label: 'Take Away' },
  { value: 'reservation', label: 'Reservasi' },
];

// reseravtion type
export const sidebar = [
  { icon: CasherIcon, label: 'Kasir' },
  { icon: ReserveIcon, label: 'Reservasi' },
  { icon: ReceiptIcon, label: 'Receipt' },
  { icon: PresentationIcon, label: 'Laporan' },
  { icon: CloseCasherIcon, label: 'Tutup Kasir' },
  { icon: SettingIcon, label: 'Settings' },
];

// field table customer name
export const headerCustomerName = ['Nama', 'No. Hp', 'Email'];

// router header navigation
export const routeNavigationHeader = [
  { path: '/table', pathBack: '/', Label: 'Meja' },
  { path: '/payment/cash', pathBack: '/', Label: 'Pembayaran' },
  { path: '/payment/bank-transfer', pathBack: '/', Label: 'Pembayaran' },
  { path: '/payment/qris', pathBack: '/', Label: 'Pembayaran' },
  { path: '/list-order', pathBack: '/', Label: 'Daftar Pesanan' },
  { path: '/receipt', pathBack: '/', Label: 'Detail Pemesanan' },
];

// table filter category
export const tableFilterCategory = [
  'Semua',
  'Available',
  'Reserved',
  'Filled',
  'Available Soon',
];

// room filter
export const roomFilter = [
  { label: 'Semua Ruangan', value: 'all-room' },
  { label: 'Ruangan VIP', value: 'vip-room' },
  { label: 'Ruangan Smooking', value: 'smooking-room' },
  { label: 'Ruangan Sofa Keluarga', value: 'sofa-room' },
  { label: 'Ruangan Sahabat Anak', value: 'child-room' },
];

// list-order filter
export const listOrderFilter = [
  { label: 'Semua', value: 'all' },
  { label: 'Pending', value: 'pending' },
  { label: 'Done', value: 'done' },
  { label: 'Cancelled', value: 'cancelled' },
];

// customer name
export const customerName = [
  { label: 'John Doe', value: 'john-doe' },
  { label: 'Jane Smith', value: 'jane-smith' },
  { label: 'Michael Johnson', value: 'michael-johnson' },
  { label: 'Emily Davis', value: 'emily-davis' },
  { label: 'Chris Brown', value: 'chris-brown' },
  { label: 'Jessica Williams', value: 'jessica-williams' },
  { label: 'David Jones', value: 'david-jones' },
  { label: 'Sarah Miller', value: 'sarah-miller' },
  { label: 'James Wilson', value: 'james-wilson' },
  { label: 'Linda Taylor', value: 'linda-taylor' },
  { label: 'Robert Anderson', value: 'robert-anderson' },
  { label: 'Patricia Thomas', value: 'patricia-thomas' },
  { label: 'Charles Jackson', value: 'charles-jackson' },
  { label: 'Barbara White', value: 'barbara-white' },
  { label: 'Paul Harris', value: 'paul-harris' },
  { label: 'Jennifer Martin', value: 'jennifer-martin' },
  { label: 'Kevin Thompson', value: 'kevin-thompson' },
  { label: 'Nancy Garcia', value: 'nancy-garcia' },
  { label: 'Jason Martinez', value: 'jason-martinez' },
  { label: 'Karen Robinson', value: 'karen-robinson' },
  { label: 'Matthew Clark', value: 'matthew-clark' },
  { label: 'Elizabeth Lewis', value: 'elizabeth-lewis' },
  { label: 'Mark Lee', value: 'mark-lee' },
  { label: 'Lisa Walker', value: 'lisa-walker' },
  { label: 'Steven Hall', value: 'steven-hall' },
  { label: 'Susan Allen', value: 'susan-allen' },
  { label: 'Thomas Young', value: 'thomas-young' },
  { label: 'Sandra King', value: 'sandra-king' },
  { label: 'Daniel Hernandez', value: 'daniel-hernandez' },
  { label: 'Donna Scott', value: 'donna-scott' },
];

export const reservationDate = [
  { day: 'Rabu', date: '16', month: 'Mei', disabled: true },
  { day: 'Sabtu', date: '23', month: 'Juni', disabled: true },
  { day: 'Kamis', date: '28', month: 'Juli', disabled: false },
  { day: 'Rabu', date: '12', month: 'Agustus', disabled: false },
  { day: 'Senin', date: '17', month: 'September', disabled: false },
  { day: 'Jumat', date: '21', month: 'Oktober', disabled: true },
  { day: 'Sabtu', date: '31', month: 'November', disabled: true },
  { day: 'Minggu', date: '8', month: 'Desember', disabled: true },
];

export const reservationTime = [
  { time: '03:00', disabled: true },
  { time: '04:00', disabled: true },
  { time: '05:00', disabled: false },
  { time: '06:00', disabled: false },
  { time: '07:00', disabled: false },
  { time: '08:00', disabled: true },
  { time: '09:00', disabled: true },
  { time: '10:00', disabled: true },
];

export const reservationDuration = [
  { duration: '1 Jam', disabled: true },
  { duration: '2 Jam', disabled: true },
  { duration: '3 Jam', disabled: false },
  { duration: '4 Jam', disabled: false },
  { duration: '5 Jam', disabled: false },
  { duration: '6 Jam', disabled: true },
  { duration: '7 Jam', disabled: true },
  { duration: '8 Jam', disabled: true },
];

export const reservationRoom = [
  { name: 'Room 1', guest: '3 Orang', floor: '1', table: '5' },
  { name: 'Room 2', guest: '2 Orang', floor: '2', table: '4' },
  { name: 'Room 3', guest: '10 Orang', floor: '3', table: '10' },
  { name: 'Room 4', guest: '6 Orang', floor: '4', table: '8' },
  { name: 'Room 5', guest: '8 Orang', floor: '5', table: '12' },
  { name: 'Room 6', guest: '4 Orang', floor: '6', table: '6' },
  { name: 'Room 7', guest: '7 Orang', floor: '7', table: '14' },
];

// tab menu payment method
export const paymentMethodType = {
  name: ['Cash', 'Transfer Bank', 'Qris'],
  pushPath: '/payment',
};

// filter payment method
export const paymentMethodTypeFilter = [
  {
    label: 'Cash',
    value: 'cash',
  },
  {
    label: 'Transfer Bank',
    value: 'transfer',
  },
  {
    label: 'Qris',
    value: 'qris',
  },
];

// filter status receipt
export const statusReceipt = [
  {
    label: 'Selesai',
    value: 'done',
  },
  {
    label: 'Pengembalian',
    value: 'return',
  },
];

// tab menu reservation
export const reservationTabMenu = {
  name: [
    'Reservasi',
    'Kedatangan',
    'Berlangsung',
    'Disajikan',
    'Selesai',
    'Ditolak',
    'Dibatalkan',
  ],
  pushPath: '/reservation',
};

// list transaction
export const listTransaction = [
  {
    orderId: 'ORD-20240403-49790',
    name: 'Rio Prayoga',
    type: 'Dine In',
    time: '10:11',
    status: 'Belum Bayar',
    table: 'Meja 01, Meja 02, Meja 03',
    price: 'Rp. 999.999.999',
  },
  {
    orderId: 'ORD-20240404-49791',
    name: 'Ahmad Dhani',
    type: 'Reservasi',
    time: '18:11',
    status: 'Sudah Bayar',
    table: 'Meja 04, Meja 05',
    price: 'Rp. 999.000.000',
  },
  {
    orderId: 'ORD-20240405-49792',
    name: 'Wildan',
    type: 'Dine In',
    time: '11:11',
    status: 'Sebagian Bayar',
    table: 'Meja 01, Meja 02, Meja 03',
    price: 'Rp. 300.000',
  },
];

// filter sort
export const filterReservation = [
  { label: 'Terbaru', value: 'newest' },
  { label: 'Terlama', value: 'oldest' },
];

// table reservation
export const reservationTableHeader = [
  'No. Pesanan',
  'Tanggal',
  'Jam',
  'Nama Pemesan',
  'No. Hp',
  'Jumlah Tamu',
  'Nama Area',
];

export const reservationTableBody = [
  {
    numberOrder: 'ord-1',
    date: 'Jumat, 17 Nov 2024',
    time: '10:00',
    name: 'Rio',
    phone: '081234567890',
    guestCount: 4,
    areaName: 'Area 1',
  },
  {
    numberOrder: 'ord-2',
    date: 'Sabtu, 18 Nov 2024',
    time: '11:00',
    name: 'Serina',
    phone: '081234567891',
    guestCount: 2,
    areaName: 'Area 2',
  },
  {
    numberOrder: 'ord-3',
    date: 'Minggu, 19 Nov 2024',
    time: '12:00',
    name: 'Budi',
    phone: '081234567892',
    guestCount: 3,
    areaName: 'Area 3',
  },
  {
    numberOrder: 'ord-4',
    date: 'Senin, 20 Nov 2024',
    time: '13:00',
    name: 'Ani',
    phone: '081234567893',
    guestCount: 5,
    areaName: 'Area 4',
  },
  {
    numberOrder: 'ord-5',
    date: 'Selasa, 21 Nov 2024',
    time: '14:00',
    name: 'Dewi',
    phone: '081234567894',
    guestCount: 2,
    areaName: 'Area 1',
  },
  {
    numberOrder: 'ord-6',
    date: 'Rabu, 22 Nov 2024',
    time: '15:00',
    name: 'Agus',
    phone: '081234567895',
    guestCount: 4,
    areaName: 'Area 2',
  },
  {
    numberOrder: 'ord-7',
    date: 'Kamis, 23 Nov 2024',
    time: '16:00',
    name: 'Maya',
    phone: '081234567896',
    guestCount: 3,
    areaName: 'Area 3',
  },
  {
    numberOrder: 'ord-8',
    date: 'Jumat, 24 Nov 2024',
    time: '17:00',
    name: 'Rina',
    phone: '081234567897',
    guestCount: 6,
    areaName: 'Area 4',
  },
  {
    numberOrder: 'ord-9',
    date: 'Sabtu, 25 Nov 2024',
    time: '18:00',
    name: 'Tono',
    phone: '081234567898',
    guestCount: 2,
    areaName: 'Area 1',
  },
  {
    numberOrder: 'ord-10',
    date: 'Minggu, 26 Nov 2024',
    time: '19:00',
    name: 'Ayu',
    phone: '081234567899',
    guestCount: 3,
    areaName: 'Area 2',
  },
  {
    numberOrder: 'ord-11',
    date: 'Senin, 27 Nov 2024',
    time: '20:00',
    name: 'Eko',
    phone: '081234567800',
    guestCount: 4,
    areaName: 'Area 3',
  },
  {
    numberOrder: 'ord-12',
    date: 'Selasa, 28 Nov 2024',
    time: '21:00',
    name: 'Dani',
    phone: '081234567801',
    guestCount: 5,
    areaName: 'Area 4',
  },
  {
    numberOrder: 'ord-13',
    date: 'Rabu, 29 Nov 2024',
    time: '22:00',
    name: 'Nina',
    phone: '081234567802',
    guestCount: 2,
    areaName: 'Area 1',
  },
  {
    numberOrder: 'ord-14',
    date: 'Kamis, 30 Nov 2024',
    time: '23:00',
    name: 'Yudi',
    phone: '081234567803',
    guestCount: 4,
    areaName: 'Area 2',
  },
  {
    numberOrder: 'ord-15',
    date: 'Jumat, 1 Des 2024',
    time: '09:00',
    name: 'Vina',
    phone: '081234567804',
    guestCount: 3,
    areaName: 'Area 3',
  },
  {
    numberOrder: 'ord-16',
    date: 'Sabtu, 2 Des 2024',
    time: '08:00',
    name: 'Ari',
    phone: '081234567805',
    guestCount: 5,
    areaName: 'Area 4',
  },
  {
    numberOrder: 'ord-17',
    date: 'Minggu, 3 Des 2024',
    time: '07:00',
    name: 'Rini',
    phone: '081234567806',
    guestCount: 2,
    areaName: 'Area 1',
  },
  {
    numberOrder: 'ord-18',
    date: 'Senin, 4 Des 2024',
    time: '06:00',
    name: 'Bima',
    phone: '081234567807',
    guestCount: 4,
    areaName: 'Area 2',
  },
  {
    numberOrder: 'ord-19',
    date: 'Selasa, 5 Des 2024',
    time: '05:00',
    name: 'Tia',
    phone: '081234567808',
    guestCount: 3,
    areaName: 'Area 3',
  },
  {
    numberOrder: 'ord-20',
    date: 'Rabu, 6 Des 2024',
    time: '04:00',
    name: 'Dino',
    phone: '081234567809',
    guestCount: 6,
    areaName: 'Area 4',
  },
];
// end  table reservation

// table receipt
export const receiptTableHeader = [
  'No. Pesanan',
  'Nama Pelanggan',
  'Tanggal',
  'Total Penjualan',
  'Staff/Karyawan',
  'Status',
];

export const receiptTableBody = [
  {
    numberOrder: 'ord-1',
    customerName: 'Rio',
    date: 'Jumat, 17 Nov 2024',
    totalSales: 150000,
    staff: 'Andi',
    status: 'Selesai',
  },
  {
    numberOrder: 'ord-2',
    customerName: 'Serina',
    date: 'Sabtu, 18 Nov 2024',
    totalSales: 200000,
    staff: 'Budi',
    status: 'Pengembalian',
  },
  {
    numberOrder: 'ord-3',
    customerName: 'Budi',
    date: 'Minggu, 19 Nov 2024',
    totalSales: 250000,
    staff: 'Citra',
    status: 'Selesai',
  },
  {
    numberOrder: 'ord-4',
    customerName: 'Ani',
    date: 'Senin, 20 Nov 2024',
    totalSales: 300000,
    staff: 'Dian',
    status: 'Pengembalian',
  },
  {
    numberOrder: 'ord-5',
    customerName: 'Dewi',
    date: 'Selasa, 21 Nov 2024',
    totalSales: 180000,
    staff: 'Eka',
    status: 'Selesai',
  },
  {
    numberOrder: 'ord-6',
    customerName: 'Agus',
    date: 'Rabu, 22 Nov 2024',
    totalSales: 220000,
    staff: 'Fajar',
    status: 'Pengembalian',
  },
  {
    numberOrder: 'ord-7',
    customerName: 'Maya',
    date: 'Kamis, 23 Nov 2024',
    totalSales: 275000,
    staff: 'Gina',
    status: 'Selesai',
  },
  {
    numberOrder: 'ord-8',
    customerName: 'Rina',
    date: 'Jumat, 24 Nov 2024',
    totalSales: 320000,
    staff: 'Heri',
    status: 'Pengembalian',
  },
  {
    numberOrder: 'ord-9',
    customerName: 'Tono',
    date: 'Sabtu, 25 Nov 2024',
    totalSales: 165000,
    staff: 'Ika',
    status: 'Selesai',
  },
  {
    numberOrder: 'ord-10',
    customerName: 'Ayu',
    date: 'Minggu, 26 Nov 2024',
    totalSales: 230000,
    staff: 'Joko',
    status: 'Pengembalian',
  },
  {
    numberOrder: 'ord-11',
    customerName: 'Eko',
    date: 'Senin, 27 Nov 2024',
    totalSales: 270000,
    staff: 'Kiki',
    status: 'Selesai',
  },
  {
    numberOrder: 'ord-12',
    customerName: 'Dani',
    date: 'Selasa, 28 Nov 2024',
    totalSales: 310000,
    staff: 'Lina',
    status: 'Pengembalian',
  },
  {
    numberOrder: 'ord-13',
    customerName: 'Nina',
    date: 'Rabu, 29 Nov 2024',
    totalSales: 160000,
    staff: 'Miko',
    status: 'Selesai',
  },
];
// end receipt
