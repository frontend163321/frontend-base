import type { Config } from 'tailwindcss';

const config = {
  darkMode: ['class'],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
  ],
  prefix: '',
  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px',
      },
    },

    extend: {
      padding: {
        content: '20px',
      },
      fontSize: {
        'heading-1': [
          '48px',
          {
            lineHeight: '60px',
            fontWeight: '600',
            letterSpacing: '0%',
          },
        ],
        'heading-2': [
          '32px',
          {
            lineHeight: '44px',
            fontWeight: '600',
            letterSpacing: '0%',
          },
        ],
        'heading-3': [
          '24px',
          {
            lineHeight: '32px',
            fontWeight: '600',
            letterSpacing: '0%',
          },
        ],
        'heading-4': [
          '20px',
          {
            lineHeight: '28px',
            fontWeight: '600',
            letterSpacing: '0%',
          },
        ],
        'heading-5': [
          '18px',
          {
            lineHeight: '26px',
            fontWeight: '600',
            letterSpacing: '0%',
          },
        ],
        'heading-6': [
          '16px',
          {
            lineHeight: '24px',
            fontWeight: '600',
            letterSpacing: '0%',
          },
        ],
        'heading-7': [
          '14px',
          {
            lineHeight: '20px',
            fontWeight: '600',
            letterSpacing: '1%',
          },
        ],
        'heading-8': [
          '12px',
          {
            lineHeight: '16px',
            fontWeight: '600',
            letterSpacing: '1%',
          },
        ],
        'heading-9': [
          '10px',
          {
            lineHeight: '14px',
            fontWeight: '600',
            letterSpacing: '1%',
          },
        ],
        'body-xxlarge': [
          '24px',
          {
            lineHeight: '32px',
            fontWeight: '400',
            letterSpacing: '0%',
          },
        ],
        'body-xlarge': [
          '20px',
          {
            lineHeight: '28px',
            fontWeight: '400',
            letterSpacing: '0%',
          },
        ],
        'body-large': [
          '18px',
          {
            lineHeight: '26px',
            fontWeight: '400',
            letterSpacing: '0%',
          },
        ],
        'body-medium': [
          '16px',
          {
            lineHeight: '24px',
            fontWeight: '400',
            letterSpacing: '0%',
          },
        ],
        'body-small': [
          '14px',
          {
            lineHeight: '20px',
            fontWeight: '400',
            letterSpacing: '0%',
          },
        ],
        'body-xsmall': [
          '12px',
          {
            lineHeight: '18px',
            fontWeight: '400',
            letterSpacing: '0%',
          },
        ],
        'body-xxsmall': [
          '10px',
          {
            lineHeight: '14px',
            fontWeight: '400',
            letterSpacing: '1%',
          },
        ],
        'body-xxxsmall': [
          '8px',
          {
            lineHeight: '10px',
            fontWeight: '400',
            letterSpacing: '1%',
          },
        ],
        'label-xxlarge': [
          '24px',
          {
            lineHeight: '32px',
            fontWeight: '500',
            letterSpacing: '0%',
          },
        ],
        'label-xlarge': [
          '20px',
          {
            lineHeight: '28px',
            fontWeight: '500',
            letterSpacing: '0%',
          },
        ],
        'label-large': [
          '18px',
          {
            lineHeight: '26px',
            fontWeight: '500',
            letterSpacing: '0%',
          },
        ],
        'label-medium': [
          '16px',
          {
            lineHeight: '24px',
            fontWeight: '500',
            letterSpacing: '0%',
          },
        ],
        'label-small': [
          '14px',
          {
            lineHeight: '20px',
            fontWeight: '500',
            letterSpacing: '0%',
          },
        ],
        'label-xsmall': [
          '12px',
          {
            lineHeight: '16px',
            fontWeight: '500',
            letterSpacing: '0%',
          },
        ],
        'label-xxsmall': [
          '10px',
          {
            lineHeight: '14px',
            fontWeight: '500',
            letterSpacing: '1%',
          },
        ],
        'label-xxxsmall': [
          '8px',
          {
            lineHeight: '10px',
            fontWeight: '500',
            letterSpacing: '1%',
          },
        ],
        'label-promo': [
          '10px',
          {
            lineHeight: '12px',
            fontWeight: '500',
            letterSpacing: '0.5%',
          },
        ],
        'button-small': [
          '12px',
          {
            lineHeight: '16px',
            fontWeight: '600',
          },
        ],
        'button-medium': [
          '14px',
          {
            lineHeight: '20px',
            fontWeight: '600',
          },
        ],
        'button-large': [
          '16px',
          {
            lineHeight: '24px',
            fontWeight: '600',
          },
        ],
      },
      colors: {
        value: 'hsl(var(--value))',
        border: 'hsl(var(--border))',
        input: 'hsl(var(--input))',
        ring: 'hsl(var(--ring))',
        background: 'hsl(var(--background))',
        foreground: 'hsl(var(--foreground))',
        'neutral-100': {
          DEFAULT: '#FAFAFA',
        },
        'neutral-200': {
          DEFAULT: '#F0F0F0',
        },
        'neutral-300': {
          DEFAULT: '#D9D9D9',
        },
        'neutral-500': {
          DEFAULT: '#808080',
        },
        'neutral-700': {
          DEFAULT: '#404040',
        },
        'neutral-800': {
          DEFAULT: '#2B2B2B',
        },
        'neutral-900': {
          DEFAULT: '#1A1A1A',
        },
        'error-50': {
          DEFAULT: '#FFF0F1',
        },
        'error-300': {
          DEFAULT: '#FF9F9F',
        },
        'error-400': {
          DEFAULT: '#FF6868',
        },
        'error-500': {
          DEFAULT: '#FB3F3F',
        },
        'error-600': {
          DEFAULT: '#E91A18',
        },
        primary: {
          DEFAULT: 'hsl(var(--primary))',
          50: '#FFFEE7',
        },
        secondary: {
          DEFAULT: 'hsl(var(--secondary))',
          50: '#FEF2F3',
          300: '#F8AAB0',
          400: '#F27A86',
          500: '#E63950',
          600: '#D42948',
        },
        info: {
          500: '#48A7F9',
        },
        success: {
          50: '#EDFDF2',
          100: '#D4F7DF',
          500: '#1CAE64',
          600: '#118C4F',
        },
        warning: {
          50: '#FFFAEC',
          100: '#FFF0C5',
          400: '#FFB222',
          500: '#F98F07',
          600: '#DD6801',
        },
        destructive: {
          DEFAULT: 'hsl(var(--destructive))',
          foreground: 'hsl(var(--destructive-foreground))',
        },
        muted: {
          DEFAULT: 'hsl(var(--muted))',
          foreground: 'hsl(var(--muted-foreground))',
        },
        accent: {
          DEFAULT: 'hsl(var(--accent))',
          foreground: 'hsl(var(--accent-foreground))',
        },
        popover: {
          DEFAULT: 'hsl(var(--popover))',
          foreground: 'hsl(var(--popover-foreground))',
        },
        card: {
          DEFAULT: 'hsl(var(--card))',
          foreground: 'hsl(var(--card-foreground))',
        },
      },
      borderRadius: {
        md: 'calc(var(--radius) - 2px)',
        sm: 'calc(var(--radius) - 4px)',
        base: 'var(--radius)',
        lg: '16px',
      },
      boxShadow: {
        top: '0px -2px 4px 0px #17171708, 0px -8px 8px 0px #17171705, 0px -18px 11px 0px #17171703, 0px -32px 13px 0px #17171700, 0px -49px 14px 0px #17171700',
        bottom:
          '0px 32px 9px 0px rgba(0, 0, 0, 0.00), 0px 21px 8px 0px rgba(0, 0, 0, 0.01), 0px 12px 7px 0px rgba(0, 0, 0, 0.02), 0px 5px 5px 0px rgba(0, 0, 0, 0.03), 0px 1px 3px 0px rgba(0, 0, 0, 0.04)',
        left: '-27px 0px 8px 0px rgba(0, 0, 0, 0.00), -17px 0px 7px 0px rgba(0, 0, 0, 0.01), -10px 0px 6px 0px rgba(0, 0, 0, 0.02), -4px 0px 4px 0px rgba(0, 0, 0, 0.03), -1px 0px 2px 0px rgba(0, 0, 0, 0.04)',
        right:
          '27px 0px 8px 0px rgba(0, 0, 0, 0.00), 17px 0px 7px 0px rgba(0, 0, 0, 0.01), 10px 0px 6px 0px rgba(0, 0, 0, 0.02), 4px 0px 4px 0px rgba(0, 0, 0, 0.03), 1px 0px 2px 0px rgba(0, 0, 0, 0.04)',
      },
      fontFamily: {
        sans: 'var(--font-general-sans)',
      },
      keyframes: {
        'accordion-down': {
          from: { height: '0' },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: '0' },
        },
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
      },
      screens: {
        smallTablet: { max: '1024px' },
      },
    },
  },
  plugins: [require('tailwindcss-animate')],
} satisfies Config;

export default config;
