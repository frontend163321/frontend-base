declare type ButtonBottom = {
  className?: string;
  title?: string | undefined;
  shadow?: boolean;
  onClick?: any;
  children?: React.ReactNode;
};
