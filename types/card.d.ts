declare type CardMenu = {
  label: boolean;
  stars: boolean;
  onClick?: () => void;
};

declare type CardDate = {
  day: string;
  date: string;
  month: string;
  disabled: boolean;
  selectedDate?: string;
  setSelectedDate?: any;
};
declare type CardTime = {
  time: string;
  duration?: boolean;
  disabled: boolean;
  selectedDate?: string;
  setSelectedDate?: any;
};

declare type CardRoom = {
  name: string;
  floor: string;
  table: string;
  guest: string;
  selectedRoom?: string;
  setSelectedRoom?: any;
};

declare type CardMenuDetail = {
  type?: 'payment' | 'order';
  countButton?: boolean;
  radio?: booelan;
  className?: string;
};
