declare type OnBoarding = {
  src: any;
  heading: string;
  description: string;
  wrapText?: boolean;
};
