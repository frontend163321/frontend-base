declare type Size = 'medium' | 'big' | 'small';

declare type CInput = {
  borderBottom?: boolean;
  type?:
    | 'text'
    | 'number'
    | 'email'
    | 'button'
    | 'radio'
    | 'password'
    | 'checkbox'
    | 'file';
  size?: Size;
  required?: boolean;
  placeholder?: string;
  field?: any;
  className?: string;
  classNameParent?: string;
  dir?: 'rtl' | 'ltr';
  value?: string | number;
  defaultValue?: string | number;
  inputRef?: refType;
  onChange?: any;
  onSelect?: any;
  icon?: any;
  iconSize?: any;
  children?: React.ReactNode;
  checked?: boolean;
  inputMode?:
    | 'none'
    | 'text'
    | 'tel'
    | 'url'
    | 'email'
    | 'numeric'
    | 'decimal'
    | 'search';
};

declare type CSelect = {
  className?: string;
  variant?: 'default' | 'secondary' | 'border_bottom';
  icon?: any;
  iconMenu?: any;
  iconSize?: number;
  placeholder: string | any;
  placeholderSize?: Size;
  chevron?: 'down' | 'right' | 'up' | 'left';
  center?: boolean;
  data: {
    value: string;
    label: string;
  }[];
  size?: 'small' | 'medium' | 'big';
  onChange?: any;
  field?: any;
  showChevron?: boolean;
};

declare type Pin = {
  value: string[];
  gap?: string;
};

declare type Image = {
  src: any;
  alt: string;
  width?: number;
  height?: number;
  className?: string;
  style?: any;
  fill?: boolean;
  roundedPlaceholder?: string;
};

declare type Numpad = {
  currCaret?: number;
  value?: string | undefined;
  setValue?: (value: string) => void;
};

declare type Sidebar = {
  open: boolean;
  setIsOpen: (open: boolean) => void;
  side?: 'left' | 'right';
};

declare type Numpad = {
  currCaret: number | string;
  setValue: (value: string) => void;
  value: string | number;
};

declare type CButton = {
  title?: string;
  icon?: any;
  rightIcon?: any;
  iconSize?: number;
  iconSizeRight?: number;
  size?: 'default' | 'large' | 'small';
  fontSize?: 'default' | 'large' | 'small';
  fontColor?: 'black' | 'gray' | 'white';
  fontWeight?: 'normal' | 'medium' | 'bold';
  type?: 'filled' | 'outline';
  variant?:
    | 'outline'
    | 'default'
    | 'secondary'
    | 'destructive'
    | 'ghost'
    | 'link';
  className?: string;
  children?: React.ReactNode;
  onClick?: () => void;
};

declare type CModal = {
  shadowFooter?: boolean;
  showFooter?: boolean;
  showHeader?: boolean;
  paddingContent?: boolean;
  headerTitle?: string;
  footerTitle?: string;
  onClickFooter?: () => void;
  open: booleheaderTitlean;
  height: string;
  width: string;
  children: React.ReactNode;
  setIsOpen: (open: boolean) => void;
};

declare type CDropdown = {
  triggerButton: React.ReactNode;
  menuContent: React.ReactNode;
  width?: string;
};

declare type CAccordion = {
  labelColor?: string;
  labelTitle: string;
  children: React.ReactNode;
  className?: string;
};

declare type PaymentMethodType = 'Cash' | 'Transfer Bank' | 'Qris';

declare type FieldDotColumn = {
  label: string;
  value: string;
  className?: string;
  classNameValue?: string;
  color?: string;
  fontSize?: string;
  valueEnd?: string;
  fontWeightEnd?: string;
  colorEnd?: string;
};

declare type HeadingPrice = {
  size?: 'big' | 'small';
  textAlign?: 'start' | 'center' | 'end';
  color?: string;
};

declare type CTable = {
  headerData: string[];
  children: React.ReactNode;
  withNumber?: boolean;
  withAction?: boolean;
};

declare type FieldColumn = {
  width?: string;
  value: string;
  label: string;
  between?: boolean;
  size?: 'default' | 'medium';
  valueColor?: string;
};
