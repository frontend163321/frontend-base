declare type ButtonFilter = {
  title: string;
  first?: boolean;
  active?: boolean;
  onClick?: () => void;
};

declare type HandleClickModal = {
  modalName: string;
  headerTitle: string;
  footerTitle?: string;
  width: string;
  height: string;
  shadowFooter?: boolean;
};

declare type TableStatus = {
  label: string;
  active: string;
  setActive: (active: string) => void;
};

declare type CardPaymentMethod = {
  name: string;
  username: string;
  nomor: string;
  icon: any;
  active: string;
  setActive: (active: string) => void;
};
