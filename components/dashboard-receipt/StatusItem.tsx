import { cn } from '@/lib/utils';

const StatusItem = ({ label }: { label: string }) => {
  const statusColor =
    label === 'Selesai'
      ? 'border-success-500 bg-success-50 text-success-500'
      : 'border-error-500 bg-error-50 text-error-500';

  return (
    <div
      className={cn(
        'w-[109px] h-[28px] text-label-small border rounded-[4px] flex justify-center items-center',
        statusColor,
      )}
    >
      {label}
    </div>
  );
};

export default StatusItem;
