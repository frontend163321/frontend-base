import LayoutMain from '../layout/LayoutMain';
import Divider from '../shared/Divider';
import FieldColumn from '../shared/FieldColumn';
import ButtonAction from './ButtonAction';

const MainContentAside = () => {
  return (
    <LayoutMain className="flex flex-col justify-between h-full pb-[99px]">
      <div>
        <div className="flex flex-col items-center justify-center">
          <h2 className="text-heading-2 text-neutral-900 mb-3">
            Rp 99.999.999
          </h2>
          <p className="text-label-medium text-neutral-500 mb-2">
            17 November 2024, 12:04
          </p>
          <p className="text-body-xlarge text-neutral-900 mb-5">
            ORD-20240403-49790
          </p>
        </div>
        <Divider color="border-neutral-300" />
        <div className="mt-5">
          <FieldColumn
            size="medium"
            width="w-[30%]"
            label="Nama Pelanggan"
            value="Noname-1234"
          />
          <FieldColumn
            size="medium"
            width="w-[30%]"
            label="Staff/Karyawan"
            value="Farhan Muhammad"
          />
          <FieldColumn
            size="medium"
            width="w-[30%]"
            label="Meja"
            value="Meja A002, Meja A003, Meja A004"
          />
          <FieldColumn
            size="medium"
            width="w-[30%]"
            label="Jumlah Tamu"
            value="12"
          />
          <FieldColumn
            size="medium"
            width="w-[30%]"
            label="Pembayaran"
            value="Cash"
          />
          <FieldColumn
            size="medium"
            width="w-[30%]"
            label="Status"
            value="Selesai"
            valueColor="text-success-500"
          />
        </div>
      </div>

      <ButtonAction />
    </LayoutMain>
  );
};

export default MainContentAside;
