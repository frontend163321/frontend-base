import CardMenuDetail from '../cards/CardMenuDetail';
import LayoutMain from '../layout/LayoutMain';
import Divider from '../shared/Divider';

const MainContentDetail = () => {
  return (
    <LayoutMain className="overflow-y-auto h-svh pb-[99px] pt-4">
      <h5 className="text-heading-5">Daftar Pemesanan</h5>
      <Divider className="border-neutral-300 mt-4" />
      {Array.from({ length: 5 }).map((_, index) => (
        <CardMenuDetail type="order" className="px-0" />
      ))}
    </LayoutMain>
  );
};

export default MainContentDetail;
