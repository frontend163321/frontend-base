import { paymentMethodTypeFilter, statusReceipt } from '@/constants';
import { CalendarIcon, SearchIcon } from '@/lib/images';
import CButton from '@components/button/CButton';
import CInput from '@components/shared/CInput';
import CSelect from '@components/shared/CSelect';

const FilterReceipt = () => {
  return (
    <div className="flex gap-3">
      <CInput
        icon={SearchIcon}
        iconSize={18}
        placeholder="Cari No. Pemesanan"
        className="!text-body-small"
        classNameParent="!w-[230px] flex-shrink-0"
        size="small"
      />
      <CButton
        icon={CalendarIcon}
        iconSize={19}
        title="16 Nov 24 - 15 Des 24"
        fontSize="small"
        className="!text-body-small w-[193px]"
        size="small"
      />
      <CSelect
        icon={false}
        placeholder={'Semua Pembayaran'}
        data={paymentMethodTypeFilter}
        className="!w-[226.67px] text-body-small text-neutral-600"
        size="small"
      />
      <CSelect
        icon={false}
        placeholder={'Semua Status'}
        data={statusReceipt}
        className="!w-[226.67px] text-body-small text-neutral-600"
        size="small"
      />
    </div>
  );
};

export default FilterReceipt;
