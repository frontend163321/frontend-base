'use client';

import { receiptTableBody, receiptTableHeader } from '@/constants';
import { setOpenModal } from '@/store/modal/action';
import { useDispatch } from 'react-redux';
import LayoutMain from '../layout/LayoutMain';
import Ctable from '../shared/Table';
import { Button } from '../ui/button';
import { TableCell, TableRow } from '../ui/table';
import FilterReceipt from './FilterReceipt';
import StatusItem from './StatusItem';

const MainContent = () => {
  const dispatch = useDispatch();
  return (
    <LayoutMain className="pt-4">
      <FilterReceipt />

      <div className="overflow-y-auto h-svh pb-52 mt-3">
        <Ctable headerData={receiptTableHeader} withNumber withAction>
          {receiptTableBody.slice(0, 10).map((item, index) => (
            <TableRow className="!py-6" key={item.numberOrder}>
              <TableCell className="font-medium">{index + 1}</TableCell>
              <TableCell className="font-medium">{item.numberOrder}</TableCell>
              <TableCell className="font-medium">{item.customerName}</TableCell>
              <TableCell className="font-medium">{item.date}</TableCell>
              <TableCell className="font-medium">{item.totalSales}</TableCell>
              <TableCell className="font-medium">{item.staff}</TableCell>
              <TableCell className="font-medium">
                <StatusItem label={item.status} />
              </TableCell>
              <TableCell className="font-medium">
                <Button
                  className="bg-neutral-200 w-20 h-10 !text-button-medium"
                  onClick={() =>
                    dispatch(
                      setOpenModal({
                        modalName: 'reservationDetailModal',
                        width: 'w-[776px]',
                        height: 'h-[699px]',
                        headerTitle: 'Detail Reservasi',
                        showFooter: false,
                      }),
                    )
                  }
                >
                  Detail
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </Ctable>
      </div>
    </LayoutMain>
  );
};

export default MainContent;
