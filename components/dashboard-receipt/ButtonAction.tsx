'use client';

import { setOpenModal } from '@/store/modal/action';
import { useDispatch } from 'react-redux';
import { Button } from '../ui/button';

const ButtonAction = () => {
  const dispath = useDispatch();
  return (
    <div className="flex gap-4">
      <Button
        className="flex-1 !text-button-large border-primary"
        variant={'outline'}
        onClick={() =>
          dispath(
            setOpenModal({
              modalName: 'cancelOrderModal',
              width: 'w-[811px]',
              height: 'h-[700px]',
              headerTitle: 'Pengembalian',
              footerTitle: 'Pengembalian Dana',
            }),
          )
        }
      >
        Pengembalian
      </Button>
      <Button className="flex-1 !text-button-large">Print</Button>
    </div>
  );
};

export default ButtonAction;
