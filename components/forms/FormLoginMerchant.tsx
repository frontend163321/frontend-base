'use client';

import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from '@/components/ui/form';
import { LoginMerchantSchema } from '@/lib/validations';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';
import * as z from 'zod';
import CInput from '../shared/CInput';

const FormLoginMerchant = () => {
  const form = useForm<z.infer<typeof LoginMerchantSchema>>({
    resolver: zodResolver(LoginMerchantSchema),
    defaultValues: {},
  });

  const onSubmit = async (values: z.infer<typeof LoginMerchantSchema>) => {
    // const payload = {
    //   phoneNumber: values.phoneNumber,
    // };
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <FormField
          control={form.control}
          name="phoneNumber"
          render={({ field }) => (
            <FormItem className="space-y-3.5">
              <FormControl>
                <CInput
                  type="number"
                  placeholder="No.Hp"
                  field={field}
                  borderBottom
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button type="submit" className="w-full mt-[24px] mb-5">
          Login
        </Button>
      </form>
    </Form>
  );
};

export default FormLoginMerchant;
