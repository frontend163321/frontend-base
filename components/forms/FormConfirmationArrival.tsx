'use client';

import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from '@/components/ui/form';
import { LoginMerchantSchema } from '@/lib/validations';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';
import * as z from 'zod';
import CInput from '../shared/CInput';

const FormConfirmationArrival = () => {
  const form = useForm<z.infer<typeof LoginMerchantSchema>>({
    resolver: zodResolver(LoginMerchantSchema),
    defaultValues: {},
  });

  const onSubmit = async (values: z.infer<typeof LoginMerchantSchema>) => {
    // const payload = {
    //   phoneNumber: values.phoneNumber,
    // };
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(onSubmit)}>
        <div className="flex flex-col gap-8">
          <FormField
            control={form.control}
            name="phoneNumber"
            render={({ field }) => (
              <FormItem className="space-y-3.5">
                <FormControl>
                  <CInput
                    type="number"
                    placeholder="Jumlah Tamu"
                    field={field}
                    borderBottom
                    required
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
        </div>

        <Button
          type="submit"
          className="w-full mt-[30px] mb-5 !text-button-large"
        >
          Simpan
        </Button>
      </form>
    </Form>
  );
};

export default FormConfirmationArrival;
