'use client';

import styles from '@styles/modules/cash.module.css';
import { Button } from '../ui/button';

const FormEnterCash = () => {
  // const inputRef: any = useRef(null);

  // useEffect(() => {
  //   inputRef.current.setSelectionRange(5, 5);
  // }, [value]);

  return (
    <>
      {/* <CInput
        inputRef={inputRef}
        placeholder="0"
        className="h-[56px] mb-3 text-[24px] font-semibold"
        value={value}
        onSelect={(e: any) => setCurrCaret(e.target.selectionStart)}
        inputMode="none"
      /> */}

      {/* <Numpad currCaret={currCaret} value={value} setValue={setValue} /> */}

      <Button className="mt-[32px]" disabled>
        Buka Kasir
      </Button>
      <div className={styles.back}>Kembali</div>
    </>
  );
};

export default FormEnterCash;
