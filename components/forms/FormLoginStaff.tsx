'use client';

import { selectData } from '@/constants';
import { HeroOnboarding1 } from '@/lib/images';
import { useState } from 'react';
import CSelect from '../shared/CSelect';
import Numpad from '../shared/Numpad';
import { Button } from '../ui/button';

const FormLoginStaff = () => {
  const [pinValue, setPinValue] = useState<string>('');

  return (
    <>
      <CSelect
        placeholder="Staff/Karyawan"
        data={selectData}
        iconMenu={HeroOnboarding1}
      />

      <h2 className="text-heading-6 mt-[24px]">PIN</h2>

      <Numpad value={pinValue} setValue={setPinValue} />
      <Button className="mt-[32px]" disabled>
        Masuk Pengkasiran
      </Button>
    </>
  );
};

export default FormLoginStaff;
