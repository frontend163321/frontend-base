'use client';

import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from '@/components/ui/form';
import {
  customerName,
  reservationDate,
  reservationDuration,
  reservationRoom,
  reservationTime,
} from '@/constants';
import { LoginMerchantSchema } from '@/lib/validations';
import { zodResolver } from '@hookform/resolvers/zod';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as z from 'zod';
import CardDate from '../cards/CardDate';
import CardTime from '../cards/CardTime';
import CInput from '../shared/CInput';
import CSelect from '../shared/CSelect';
import CardRoom from '../cards/CardRoom';

const FormEditReservation = () => {
  const [selectedDate, setSelectedDate] = useState('');
  const [selectedTime, setSelectedTime] = useState('');
  const [selectedRoom, setSelectedRoom] = useState('');

  const form = useForm<z.infer<typeof LoginMerchantSchema>>({
    resolver: zodResolver(LoginMerchantSchema),
    defaultValues: {},
  });

  const onSubmit = async (values: z.infer<typeof LoginMerchantSchema>) => {
    // const payload = {
    //   phoneNumber: values.phoneNumber,
    // };
  };

  return (
    <Form {...form}>
      <form className="h-full relative" onSubmit={form.handleSubmit(onSubmit)}>
        <label className="text-label-small text-neutral-900">
          Informasi Pelanggan
        </label>

        <div className="flex flex-col gap-8 h-[69%] overflow-y-auto mt-4">
          <FormField
            control={form.control}
            name="phoneNumber"
            render={({ field }) => (
              <FormItem className="space-y-3.5">
                <FormControl>
                  <CSelect
                    data={customerName}
                    placeholder={'Nama Pelanggan'}
                    size="medium"
                    className="p-0"
                    field={field}
                    icon={false}
                    chevron="right"
                    variant="border_bottom"
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="phoneNumber"
            render={({ field }) => (
              <FormItem className="space-y-3.5">
                <FormControl>
                  <CInput
                    type="number"
                    placeholder="Jumlah Tamu"
                    field={field}
                    borderBottom
                    required
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="phoneNumber"
            render={({ field }) => (
              <FormItem className="space-y-3.5">
                <FormControl>
                  <CInput
                    type="number"
                    placeholder="No. Hp"
                    field={field}
                    borderBottom
                    required
                  />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <div className="flex flex-col gap-4">
            <label className="text-label-small text-neutral-900">Jadwal</label>

            {/* date */}
            <div>
              <label className="text-neutral-900 text-body-medium">
                Pilih Tanggal
              </label>

              <div className="flex gap-2 overflow-x-auto mt-2">
                {reservationDate.map((item, index) => (
                  <CardDate
                    date={item.date}
                    day={item.day}
                    month={item.month}
                    disabled={item.disabled}
                    selectedDate={selectedDate}
                    setSelectedDate={setSelectedDate}
                  />
                ))}
              </div>
            </div>

            {/* time */}
            <div>
              <label className="text-neutral-900 text-body-medium">
                Pilih Jam
              </label>

              <div className="flex gap-2 overflow-x-auto mt-2">
                {reservationTime.map((item, index) => (
                  <CardTime
                    time={item.time}
                    disabled={item.disabled}
                    selectedDate={selectedTime}
                    setSelectedDate={setSelectedTime}
                  />
                ))}
              </div>
            </div>

            {/* duration */}
            <div>
              <label className="text-neutral-900 text-body-medium">
                Pilih Durasi
              </label>

              <div className="flex gap-2 overflow-x-auto mt-2">
                {reservationDuration.map((item, index) => (
                  <CardTime
                    time={item.duration}
                    disabled={item.disabled}
                    selectedDate={selectedTime}
                    setSelectedDate={setSelectedTime}
                  />
                ))}
              </div>
            </div>

            {/* room */}
            <div>
              <label className="text-neutral-900 text-body-medium">
                Pilih Ruangan
              </label>

              <div className="flex flex-col gap-2 mt-2">
                {reservationRoom.map((item, index) => (
                  <CardRoom
                    name={item.name}
                    guest={item.guest}
                    table={item.table}
                    floor={item.floor}
                    selectedRoom={selectedRoom}
                    setSelectedRoom={setSelectedRoom}
                  />
                ))}
              </div>
            </div>
          </div>

          {/* another */}
          <div>
            <label className="text-label-small text-neutral-900">Lainnya</label>
            <div className="flex flex-col gap-8 mt-4">
              <FormField
                control={form.control}
                name="phoneNumber"
                render={({ field }) => (
                  <FormItem className="space-y-3.5">
                    <FormControl>
                      <CInput
                        type="text"
                        placeholder="Nama Acara"
                        field={field}
                        borderBottom
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name="phoneNumber"
                render={({ field }) => (
                  <FormItem className="space-y-3.5">
                    <FormControl>
                      <CInput
                        type="text"
                        placeholder="Catatan"
                        field={field}
                        borderBottom
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />
            </div>
          </div>
        </div>

        <Button
          type="submit"
          className="absolute bottom-0 w-full mt-[30px] !text-button-large"
        >
          Tambah
        </Button>
      </form>
    </Form>
  );
};

export default FormEditReservation;
