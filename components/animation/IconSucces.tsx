'use client';

import React from 'react';
import Lottie from 'lottie-react';
import icSuccess from '@/public/animation/ic_success_animation.json';

const IconSucces = () => {
  return (
    <Lottie animationData={icSuccess} loop={true} style={{ width: '102px' }} />
  );
};

export default IconSucces;
