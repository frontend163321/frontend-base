import Divider from '../shared/Divider';

const DividerTicket = () => {
  return (
    <div className="relative flex justify-between w-full items-center mt-4">
      <div className="absolute left-[-27px] bg-neutral-100 w-8 h-8 rounded-full flex-shrink-0" />
      <div className="absolute right-[-27px] bg-neutral-100 w-8 h-8 rounded-full flex-shrink-0" />
      <div className="w-full px-5">
        <Divider className="w-full" type="dashed" color="border-neutral-300" />
      </div>
    </div>
  );
};

export default DividerTicket;
