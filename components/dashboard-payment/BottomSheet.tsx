'use client';

import React from 'react';
import ButtonBottom from '../button/ButtonBottom';
import { useDispatch } from 'react-redux';
import { setOpenModal } from '@/store/modal/action';

const BottomSheet = () => {
  const dispatch = useDispatch();

  return (
    <ButtonBottom
      title="Bayar"
      className="!absolute !z-0 !bottom-20"
      shadow
      onClick={() =>
        dispatch(
          setOpenModal({
            modalName: 'successPaymentModal',
            width: 'w-[745px]',
            height: 'h-[697px]',
            showFooter: false,
            showHeader: false,
            paddingContent: false,
          }),
        )
      }
    />
  );
};

export default BottomSheet;
