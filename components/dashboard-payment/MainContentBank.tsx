'use client';

import { BCAIcon, MandiriIcon, MyBankIcon } from '@/lib/images';
import { useState } from 'react';
import CardPaymentMethod from './CardPaymentMethod';

const MainContentBankTransfer = () => {
  const [active, setActive] = useState<string>('');

  return (
    <div className="grid grid-cols-2 gap-3 mt-7">
      <CardPaymentMethod
        name="Mandiri"
        icon={MandiriIcon}
        username="a.n Rio Prayoga Prayoga teja putra"
        nomor="1234567890"
        active={active}
        setActive={setActive}
      />
      <CardPaymentMethod
        name="BCA"
        icon={BCAIcon}
        username="a.n Rio Prayoga Prayoga teja putra"
        nomor="1234567890"
        active={active}
        setActive={setActive}
      />
      <CardPaymentMethod
        name="Mybank"
        icon={MyBankIcon}
        username="a.n Rio Prayoga Prayoga teja putra"
        nomor="1234567890"
        active={active}
        setActive={setActive}
      />
    </div>
  );
};

export default MainContentBankTransfer;
