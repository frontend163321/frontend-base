import { cn } from '@/lib/utils';
import React from 'react';

const HeadingPrice = ({ textAlign, color, size = 'big' }: HeadingPrice) => {
  return (
    <div
      className={cn(
        'flex bg-neutral-200  mb-6 items-center px-6 rounded-base',
        textAlign ? `justify-${[textAlign]}` : 'justify-center',
        size === 'big' ? 'h-[68px]' : 'h-[52px]',
      )}
    >
      <h2
        className={cn(
          size === 'big' ? '!text-heading-2' : '!text-heading-4',
          color || 'text-secondary',
        )}
      >
        Rp 999.999.999
      </h2>
    </div>
  );
};

export default HeadingPrice;
