import React from 'react';
import Divider from '../shared/Divider';
import FieldColumn from '../shared/FieldColumn';
import LayoutMain from '../layout/LayoutMain';

const InformationOrder = () => {
  return (
    <LayoutMain>
      <div className="flex justify-between">
        <div>
          <h3 className="text-label-medium text-neutral-900 mb-1">
            ORD-20240403-49790
          </h3>
          <p className="text-body-xsmall text-neutral-600">
            16 Mei 2023 - 10:15
          </p>
        </div>
        <span className="text-label-medium text-neutral-900">Dine in</span>
      </div>

      <Divider color="border-neutral-300" className="mt-2 mb-[10px]" />

      <div className="flex flex-col gap-1">
        <FieldColumn
          width="w-[33%]"
          label="Meja"
          value="Meja A002, Meja A003, Meja A004, Meja A004"
        />
        <FieldColumn
          width="w-[33%]"
          label="Nama Pelanggan"
          value="Noname-1234"
        />
        <FieldColumn width="w-[33%]" label="Jumlah Tamu" value="12" />
      </div>
    </LayoutMain>
  );
};

export default InformationOrder;
