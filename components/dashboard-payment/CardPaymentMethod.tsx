import Image from 'next/image';
import CInput from '../shared/CInput';
import FieldDotColumn from '../shared/FieldDotColumn';
import { cn } from '@/lib/utils';

const CardPaymentMethod = ({
  name,
  username,
  nomor,
  icon,
  active,
  setActive,
}: CardPaymentMethod) => {
  const bgIcon =
    name === 'BCA'
      ? 'bg-[#0060AF]'
      : name === 'Mandiri'
        ? 'bg-[#003D79]'
        : 'bg-[#FFCE4C]';

  return (
    <div
      className={cn(
        'p-3 flex gap-[16px] border rounded-base border-neutral-300',
        active === name && 'border-primary',
      )}
      onClick={() => setActive(name)}
    >
      <div
        className={cn(
          'flex justify-center items-center h-[44px] w-[62px] p-[4.4px] rounded-[4px] flex-shrink-0',
          bgIcon,
        )}
      >
        <Image src={icon} alt="mandiri" />
      </div>

      <div className="w-full flex gap-2 items-center justify-between">
        <div>
          <FieldDotColumn
            fontSize="text-body-xsmall"
            color="text-neutral-900"
            label={name}
            className="mb-0"
            classNameValue="line-clamp-1"
            value={username}
          />
          <h6 className="text-heading-6">{nomor}</h6>
        </div>

        <div>
          <CInput type="radio" className="w-6 h-6" checked={active === name} />
        </div>
      </div>
    </div>
  );
};

export default CardPaymentMethod;
