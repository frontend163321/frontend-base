import HeadingPrice from '@/components/dashboard-payment/HeadingPrice';
import MainContentBankTransfer from '@/components/dashboard-payment/MainContentBank';
import MainContentCash from '@/components/dashboard-payment/MainContentCash';
import MainContentQRIS from '@/components/dashboard-payment/MainContentQRIS';
import LayoutMain from '@/components/layout/LayoutMain';
import MenuBar from '@/components/shared/MenuBar';
import { paymentMethodType } from '@/constants';

import { notFound } from 'next/navigation';

const WrapMainContent = ({ slug }: { slug: string }) => {
  const MainContent = () => {
    switch (slug) {
      case 'cash':
        return <MainContentCash />;
      case 'bank-transfer':
        return <MainContentBankTransfer />;
      case 'qris':
        return <MainContentQRIS />;
      default:
        return notFound();
    }
  };

  return (
    <LayoutMain className="h-full">
      <HeadingPrice />
      <div className="h-[70%] overflow-y-auto">
        <h5 className="text-heading-5 mb-3">Pilih Metode Pembayaran</h5>
        <MenuBar data={paymentMethodType} active={slug} />
        <div className="pb-12">
          <MainContent />
        </div>
      </div>
    </LayoutMain>
  );
};

export default WrapMainContent;
