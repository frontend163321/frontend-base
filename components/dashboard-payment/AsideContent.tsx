import CardMenuDetail from '../cards/CardMenuDetail';
import LayoutMain from '../layout/LayoutMain';
import InformationOrder from './InformationOrder';

const AsidePaymentContent = () => {
  return (
    <>
      <InformationOrder />
      <div className="py-4 h-screen bg-neutral-200">
        <LayoutMain className="py-0 h-full">
          <h4 className="text-heading-7 text-neutral-900">Detail Pesanan</h4>

          <div className="flex flex-col mt-3 gap-2 h-full overflow-y-auto pb-96">
            {Array.from({ length: 15 }).map((_, index) => (
              <CardMenuDetail />
            ))}
          </div>
        </LayoutMain>
      </div>
    </>
  );
};

export default AsidePaymentContent;
