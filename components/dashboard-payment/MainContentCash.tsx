'use client';

import HeadingPrice from './HeadingPrice';

const MainContentCash = () => {
  return (
    <>
      <div className="grid grid-cols-2 mt-6 gap-3">
        <div className="h-10 flex justify-center items-center bg-neutral-200 rounded-base text-label-medium text-neutral-700">
          Uang Pas
        </div>
        <div className="h-10 flex justify-center items-center bg-neutral-200 rounded-base text-label-medium text-neutral-700">
          1.000.0000.000
        </div>
      </div>

      <div className="mt-[16px]">
        <HeadingPrice textAlign="end" color="text-neutral-900" />
        {/* <Numpad /> */}
      </div>
    </>
  );
};

export default MainContentCash;
