import { QRCodeSVG } from 'qrcode.react';

const MainContentQRIS = () => {
  return (
    <div className="flex flex-col items-center justify-center mt-6">
      <div className="w-[240px]">
        <QRCodeSVG value="rio ganteng" width={240} height={240} />
        <div className="flex justify-between mt-6">
          <p className="text-body-medium text-neutral-900">
            Kode ini akan kedaluarsa
          </p>
          <span className="text-heading-6 text-neutral-900">00:56</span>
        </div>
      </div>
    </div>
  );
};

export default MainContentQRIS;
