'use client';

import { XLSXIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import styles from '@styles/components/header.module.css';
import { useState } from 'react';
import ButtonMenu from '../button/ButtonMenu';
import CButton from '../button/CButton';
import Sidebar from '../shared/Sidebar';

const HeaderReceipt = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOpenSidebar = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <header className={styles.header}>
        <div className={cn('items-center', styles.left)}>
          {/* humburger menu */}
          <ButtonMenu handleOpenSidebar={handleOpenSidebar} />

          {/* Title */}
          <h4 className="text-heading-4 text-neutral-900">Receipt</h4>
        </div>

        <div className={styles.right}>
          <CButton
            icon={XLSXIcon}
            title="Eksport"
            fontSize="small"
            className="!text-button-medium w-[117px] !bg-primary"
            size="small"
            iconSize={14}
            fontColor="black"
          />
        </div>
      </header>

      <Sidebar open={isOpen} setIsOpen={setIsOpen} />
    </>
  );
};

export default HeaderReceipt;
