'use client';

import { rightHeader } from '@/constants';
import { SearchIcon } from '@/lib/images';
import styles from '@styles/components/header.module.css';
import Image from 'next/image';
import { useState } from 'react';
import ButtonMenu from '../button/ButtonMenu';
import CButton from '../button/CButton';
import CInput from '../shared/CInput';
import Sidebar from '../shared/Sidebar';

const HeaderCashier = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOpenSidebar = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <header className={styles.header}>
        <div className={styles.left}>
          {/* humburger menu */}
          <ButtonMenu handleOpenSidebar={handleOpenSidebar} />

          {/* search input */}
          <CInput icon={SearchIcon} iconSize={20} placeholder="Cari Menu" />
        </div>

        <div className={styles.right}>
          {rightHeader.map(item => (
            <CButton type="filled" className={`${!item?.label && '!p-[10px]'}`}>
              <div className={`${styles.buttonItem}`}>
                <Image src={item.icon} alt="list order icon" />
                {item.label}
              </div>
            </CButton>
          ))}
        </div>
      </header>

      <Sidebar open={isOpen} setIsOpen={setIsOpen} />
    </>
  );
};

export default HeaderCashier;
