'use client';

import { filterReservation } from '@/constants';
import { CalendarIcon, FilterIcon, SearchIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import styles from '@styles/components/header.module.css';
import { useState } from 'react';
import ButtonMenu from '../button/ButtonMenu';
import CButton from '../button/CButton';
import CInput from '../shared/CInput';
import CSelect from '../shared/CSelect';
import Sidebar from '../shared/Sidebar';

const HeaderReservation = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const handleOpenSidebar = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <header className={styles.header}>
        <div className={cn('items-center', styles.left)}>
          {/* humburger menu */}
          <ButtonMenu handleOpenSidebar={handleOpenSidebar} />

          {/* Title */}
          <h4 className="text-heading-4 text-neutral-900">Reservasi</h4>
        </div>

        <div className={styles.right}>
          <CInput
            icon={SearchIcon}
            iconSize={20}
            placeholder="Cari Menu"
            className="!text-body-small"
            classNameParent="!w-[230px] flex-shrink-0"
            size="small"
          />

          <div className="flex gap-3">
            <CSelect
              icon={FilterIcon}
              placeholder={'Urutkan'}
              data={filterReservation}
              showChevron={false}
              className="!w-[130px] text-body-small text-neutral-600"
              size="small"
            />
            <CButton
              icon={CalendarIcon}
              title="17 Nov 2024"
              fontSize="small"
              className="!font-normal w-[138px]"
              size="small"
            />
          </div>
        </div>
      </header>

      <Sidebar open={isOpen} setIsOpen={setIsOpen} />
    </>
  );
};

export default HeaderReservation;
