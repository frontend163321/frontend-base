import { cn } from '@/lib/utils';
import styles from '@styles/modules/dashboard.module.css';

const LayoutContent = ({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) => {
  return <div className={cn(styles.layoutContent, className)}>{children}</div>;
};

export default LayoutContent;
