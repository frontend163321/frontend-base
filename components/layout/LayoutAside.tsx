import { cn } from '@/lib/utils';

const LayoutAside = ({
  shadow = 'left',
  children,
}: {
  shadow?: 'left' | 'right' | boolean;
  children: React.ReactNode;
}) => {
  return (
    <div
      className={cn('!z-20', shadow ? `shadow-${shadow}` : 'bg-neutral-200')}
    >
      {children}
    </div>
  );
};

export default LayoutAside;
