import { cn } from '@/lib/utils';
import React from 'react';

const LayoutMain = ({
  className,
  children,
}: {
  className?: string;
  children: React.ReactNode;
}) => {
  return <div className={cn('p-5', className)}>{children}</div>;
};

export default LayoutMain;
