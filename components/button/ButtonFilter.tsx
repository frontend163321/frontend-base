import React from 'react';
import { Button } from '../ui/button';
import styles from '@styles/components/button.module.css';

const ButtonFilter = ({
  title,
  first,
  onClick,
  active = false,
}: ButtonFilter) => {
  const _className = active
    ? `${first && 'ms-5'} ${styles.activeFilter}`
    : `${first && 'ms-5'} ${styles.inactiveFilter}`;

  return (
    <Button
      variant={active ? 'secondary' : 'outline'}
      className={_className}
      onClick={onClick}
    >
      {title}
    </Button>
  );
};

export default ButtonFilter;
