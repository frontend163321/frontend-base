import styles from '@styles/components/cbutton.module.css';
import { Button } from '../ui/button';
import Image from 'next/image';

const CButton = ({
  children,
  onClick,
  className,
  type,
  size,
  variant,
  title,
  icon,
  rightIcon,
  iconSizeRight,
  iconSize,
  fontSize,
  fontColor,
  fontWeight = 'medium',
}: CButton) => {
  const _type = () => {
    switch (type) {
      case 'filled':
        return 'h-10 px-5 bg-neutral-200';
      default:
        return 'bg-primary';
    }
  };

  const _size = () => {
    switch (size) {
      case 'large':
        return '!py-6 !px-3';
      case 'small':
        return '!py-2 !px-2';
      default:
        return '';
    }
  };

  const _fontSize = () => {
    switch (fontSize) {
      case 'small':
        return 'text-[14px]';
      case 'large':
        return 'text-[18px]';
      default:
        return 'text-[16px]';
    }
  };

  const _fontColor = () => {
    switch (fontColor) {
      case 'white':
        return '!text-neutral-100';
      case 'black':
        return 'text-neutral-900';
      case 'gray':
        return 'text-neutral-600';
      default:
        return 'text-neutral-600';
    }
  };

  const _fontWeight = () => {
    switch (fontWeight) {
      case 'bold':
        return 'font-bold';
      case 'normal':
        return 'font-[600px]';
      default:
        return fontWeight;
    }
  };

  return (
    <Button
      variant={variant}
      className={`${styles.button} ${className} ${type === 'filled' ? _type() : ''} ${_size()} ${variant === 'ghost' ? 'p-0 m-0 h-0' : ''}`}
      onClick={onClick}
    >
      <>
        {icon && (
          <Image
            src={icon}
            alt="profile icon"
            width={iconSize || 25}
            height={iconSize || 25}
            className={`${title ? 'me-2' : ''}`}
          />
        )}
        {title && (
          <div className={`${_fontSize()} ${_fontColor()} ${_fontWeight()}`}>
            {title}
          </div>
        )}

        {rightIcon && (
          <div className="flex justify-end w-full">
            <Image
              src={rightIcon}
              alt="profile icon"
              width={iconSizeRight || 25}
              height={iconSizeRight || 25}
            />
          </div>
        )}

        {children && children}
      </>
    </Button>
  );
};

export default CButton;
