import { MenuIcon } from '@/lib/images';
import CButton from './CButton';

const ButtonMenu = ({
  handleOpenSidebar,
}: {
  handleOpenSidebar: () => void;
}) => {
  return (
    <CButton
      icon={MenuIcon}
      type="filled"
      onClick={handleOpenSidebar}
      className="w-[40px] !p-0"
      iconSize={24}
    />
  );
};

export default ButtonMenu;
