import React from 'react';
import styles from '@styles/components/button.module.css';
import { Button } from '../ui/button';
import { cn } from '@/lib/utils';

const ButtonBottom = ({
  title,
  shadow,
  onClick,
  className,
  children,
}: ButtonBottom) => {
  return (
    <div
      className={cn(styles.buttonBottom, shadow ? 'shadow-top' : '', className)}
    >
      {children && <div className="w-full flex gap-3">{children}</div>}
      {!children && (
        <Button className="w-full !text-button-large" onClick={onClick}>
          {title}
        </Button>
      )}
    </div>
  );
};

export default ButtonBottom;
