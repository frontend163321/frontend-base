import { TripleDotsIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import AccordionMenuDetail from '@components/dashboard-cashier/AccordionMenuDetail';
import DropdownActionMenu from '@components/dashboard-cashier/DropdownActionMenu';
import CAccordion from '@components/shared/CAccordion';
import CDropdown from '@components/shared/CDropdown';
import Divider from '@components/shared/Divider';
import Image from 'next/image';
import CountTotal from '../shared/CountTotal';
import CInput from '../shared/CInput';

const CardMenuDetail = ({
  type = 'payment',
  radio,
  countButton,
  className,
}: CardMenuDetail) => {
  return (
    <>
      <div className="flex items-start gap-[30px]">
        <div
          className={cn(
            'w-full flex !min-h-[108px] bg-neutral-100 p-3',
            type === 'payment' &&
              'border border-neutral-300 rounded-base !min-h-[108px]',
            !countButton && 'flex-shrink-0',
            countButton && 'col-span-2',
            className,
          )}
        >
          <div className={cn(radio ? 'w-[80px]' : 'w-[28px]')}>
            <div className="flex gap-3">
              {radio && (
                <div className="pb-">
                  <CInput type="radio" className="w-6 mt-[-5px]" />
                </div>
              )}
              <h6
                className={cn(
                  '!text-heading-5 text-secondary',
                  type === 'payment' && '!text-heading-6',
                )}
              >
                1x
              </h6>
            </div>
          </div>
          <div className="w-full">
            <div className="flex justify-between">
              <label
                className={cn(
                  'text-label-large !text-neutral-900 line-clamp-1 mb-1',
                  type === 'payment' && 'text-label-medium',
                )}
              >
                Iga Bakar Special Rendang Jawa
              </label>
              {type === 'payment' && (
                <CDropdown
                  triggerButton={<Image src={TripleDotsIcon} alt="menu" />}
                  menuContent={<DropdownActionMenu />}
                />
              )}
              {type === 'order' && (
                <p className="text-heading-6 text-neutral-700">Rp 123.000</p>
              )}
            </div>

            {type === 'order' && <AccordionMenuDetail type="order" />}

            {type === 'payment' && (
              <div>
                <p className="text-body-small text-neutral-700 pb-[14px]">
                  Rp 123.000
                </p>
                <CAccordion labelTitle="Lihat Detail" className="py-0">
                  <AccordionMenuDetail />
                </CAccordion>
              </div>
            )}
          </div>
        </div>
        {countButton && (
          <div className="flex-shrink-0 py-4">
            <CountTotal type="order" />
          </div>
        )}
      </div>

      {type === 'order' && <Divider color="border-neutral-300 pt-1" />}
    </>
  );
};

export default CardMenuDetail;
