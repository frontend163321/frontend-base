import { CheckedLabel, PeopleIcon, TableIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import Image from 'next/image';

const CardRoom = ({
  name,
  guest,
  floor,
  table,
  selectedRoom,
  setSelectedRoom,
}: CardRoom) => {
  const LabelIcon = ({ icon, label }: { icon: any; label: string }) => {
    return (
      <div className="flex items-center gap-[6px]">
        <Image src={icon} alt="icon" />
        <p className="text-body-small text-neutral-700">{label}</p>
      </div>
    );
  };

  return (
    <div
      className={cn(
        'relative flex gap-3 w-full h-[66px] bg-neutral-200 rounded-base overflow-hidden',
        selectedRoom === name && 'border border-secondary-500',
      )}
      onClick={() => setSelectedRoom(name)}
    >
      {selectedRoom === name && (
        <Image
          src={CheckedLabel}
          alt="checked"
          className="absolute top-[-2px] right-[-2px]"
        />
      )}
      <div className="relative h-full w-[88px] rounded-base overflow-hidden">
        <Image src="/images/photos/thumbnail.png" alt="room thumbnail" fill />
      </div>
      <div className="flex flex-col justify-center gap-1">
        <label className="text-label-medium text-neutral-900">{name}</label>
        <div className="flex items-center gap-4">
          <LabelIcon icon={TableIcon} label={floor} />
          <LabelIcon icon={TableIcon} label={table} />
          <LabelIcon icon={PeopleIcon} label={guest} />
        </div>
      </div>
    </div>
  );
};

export default CardRoom;
