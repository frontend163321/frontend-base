import { ClockIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import Image from 'next/image';
import React from 'react';

const DownLabel = ({ label, value }: { label: string; value: string }) => {
  return (
    <div>
      <label className="text-label-xxsmall text-neutral-600">{label}</label>
      <h3 className="mt-[-5px] text-body-medium !text-neutral-900">{value}</h3>
    </div>
  );
};

const CardTable = ({
  type,
  selectedTable,
  setSelectedTable,
}: {
  type: string;
  selectedTable: string[];
  setSelectedTable: (value: string[]) => void;
}) => {
  const borderColor: any = {
    reserved: 'border-error-500',
    available: `border-success-500`,
    filled: `border-neutral-500`,
    'available soon': `!border-warning-500`,
  };

  const wrapLabelColor: any = {
    reserved: 'bg-error-50 border-error-400',
    available: `bg-success-50 border-success-500`,
    filled: `bg-neutral-200 border-neutral-400`,
    'available soon': `bg-warning-50 border-warning-400`,
  };

  const smallLabelColor: any = {
    reserved: 'text-error-400',
    available: `text-success-500`,
    filled: `text-neutral-600`,
    'available soon': `text-warning-400`,
  };

  const mediumLabelColor: any = {
    reserved: 'text-error-600',
    available: `text-success-600`,
    filled: `text-neutral-700`,
    'available soon': `text-warning-600`,
  };

  return (
    <div
      className={cn(
        'flex flex-col justify-between border rounded-base h-[134px] p-3',
        borderColor?.[type.toLowerCase()],
        selectedTable.includes('Meja A300') &&
          type.toLowerCase() === 'available' &&
          'bg-success-100',
      )}
      onClick={() =>
        type?.toLowerCase() === 'available' &&
        setSelectedTable([...selectedTable, 'Meja A300'])
      }
    >
      <div className="flex items-start justify-between gap-3">
        <div
          className={cn(
            'w-full flex flex-col items-center p-[3px] border rounded-[4px]',
            wrapLabelColor?.[type.toLowerCase()],
          )}
        >
          <label
            className={cn(
              '!text-label-xxxsmall',
              smallLabelColor?.[type.toLowerCase()],
            )}
          >
            Ruang VIP
          </label>
          <h3
            className={cn(
              '!text-label-medium',
              mediumLabelColor?.[type.toLowerCase()],
            )}
          >
            Meja A300
          </h3>
        </div>

        {false && (
          <div className="flex items-center gap-1 flex-shrink-0">
            <Image src={ClockIcon} alt="clock icon" width={12} height={12} />
            <span className="text-label-xsmall text-neutral-700">00:12</span>
          </div>
        )}
      </div>

      <div className="flex justify-between">
        <DownLabel label="Kapasitas" value="4 Orang" />
        <DownLabel label="Jenis" value="Lesahan" />
      </div>
    </div>
  );
};

export default CardTable;
