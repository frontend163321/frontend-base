import { cn } from '@/lib/utils';
import React from 'react';

const CardTime = ({
  time,
  duration,
  disabled,
  selectedDate,
  setSelectedDate,
}: CardTime) => {
  return (
    <div
      className={cn(
        'flex flex-col items-center justify-center h-[48px] bg-neutral-200 rounded-[4px] flex-shrink-0',
        duration ? 'w-[85px]' : 'w-[82px]',
        disabled && 'bg-neutral-300',
        selectedDate === time && 'bg-secondary-50 border-secondary-500 border',
      )}
      onClick={() => !disabled && setSelectedDate(time)}
    >
      <label
        className={cn(
          '!body-text-medium text-neutral-900',
          disabled && 'text-neutral-600',
          selectedDate === time && 'text-secondary-600',
        )}
      >
        {time}
      </label>
    </div>
  );
};

export default CardTime;
