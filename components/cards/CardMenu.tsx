import { FoodThumbnail } from '@/lib/images';
import styles from '@styles/components/card.module.css';
import CImage from '../shared/CImage';
import Label from '../shared/Label';
import LabelStars from '../shared/LabelStars';

const CardMenu = ({ label = false, stars = false, onClick }: CardMenu) => {
  return (
    <div
      className={`${styles.cardMenu} ${styles.cardShadow}`}
      onClick={onClick}
    >
      {/* image */}
      <div className={styles.cardMenuThumbnail}>
        <CImage
          src={FoodThumbnail}
          alt="thumbnail"
          roundedPlaceholder="rounded-base"
          fill
        />

        {label && <Label />}
        {stars && <LabelStars />}
      </div>

      {/* content */}
      <div className="p-2">
        <h1>Iga Bakar Special Rendang Jawa</h1>
        <div>
          <h2>Rp 45.000</h2>
          <span>Rp 50.000</span>
        </div>
      </div>
    </div>
  );
};

export default CardMenu;
