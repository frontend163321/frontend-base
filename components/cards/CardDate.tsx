import { cn } from '@/lib/utils';
import React from 'react';

const CardDate = ({
  day,
  date,
  month,
  disabled,
  selectedDate,
  setSelectedDate,
}: CardDate) => {
  return (
    <div
      className={cn(
        'flex flex-col items-center justify-center w-[68px] h-[68px] bg-neutral-200 rounded-[4px] flex-shrink-0',
        disabled && 'bg-neutral-300',
        selectedDate === day && 'bg-secondary-50 border-secondary-500 border',
      )}
      onClick={() => !disabled && setSelectedDate(day)}
    >
      <p
        className={cn(
          '!text-body-xxsmall text-neutral-500',
          selectedDate === day && 'text-secondary-400',
        )}
      >
        {day}
      </p>
      <label
        className={cn(
          '!text-label-medium text-neutral-900',
          disabled && 'text-neutral-600',
          selectedDate === day && 'text-secondary-600',
        )}
      >
        {date}
      </label>
      <p
        className={cn(
          '!text-body-xxsmall text-neutral-500',
          selectedDate === day && 'text-secondary-400',
        )}
      >
        {month}
      </p>
    </div>
  );
};

export default CardDate;
