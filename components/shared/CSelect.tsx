import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { ProfileIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import styles from '@/styles/components/cselect.module.css';
import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ChevronUpIcon,
} from '@radix-ui/react-icons';
import Image from 'next/image';
import Divider from './Divider';

const CSelect = ({
  className,
  placeholder,
  data,
  icon = ProfileIcon,
  iconMenu = false,
  iconSize,
  center,
  variant,
  chevron,
  size = 'big',
  onChange,
  field,
  showChevron = true,
}: CSelect) => {
  const ChevronIcon = () => {
    const color = variant === 'secondary' ? '#E63950' : '#595959';

    switch (chevron) {
      case 'left':
        return <ChevronLeftIcon color={color} width={23} height={23} />;

      case 'right':
        return <ChevronRightIcon color={color} width={23} height={23} />;

      case 'up':
        return <ChevronUpIcon color={color} width={23} height={23} />;

      default:
        return <ChevronDownIcon color={color} width={23} height={23} />;
    }
  };

  const _size = size === 'big' ? 'h-[48px]' : 'h-[40px]';
  const fontSize = size === 'big' ? 'text-base' : 'text-sm';

  return (
    <>
      <Select onValueChange={onChange} {...field}>
        <SelectTrigger
          className={cn(
            variant === 'border_bottom' && 'bg-transparent border-none',
            center ? 'justify-center' : 'justify-between',
            variant === 'secondary' && styles.secondarySelect,
            className,
            _size,
            fontSize,
          )}
        >
          <div
            className={cn(
              'w-full',
              styles.trigger,
              variant == 'secondary' && '!justify-center !gap-1',
            )}
          >
            <div
              className={cn(
                'w-full',
                styles.wrapSelect,
                variant === 'border_bottom' && 'border-none',
              )}
            >
              {icon && (
                <Image
                  src={icon}
                  alt="profile icon"
                  width={iconSize || 25}
                  height={iconSize || 25}
                  className="flex-shrink-0 me-2"
                />
              )}
              <SelectValue placeholder={placeholder} />
            </div>

            {showChevron && <ChevronIcon />}
          </div>
        </SelectTrigger>
        <SelectContent>
          {data?.map((item: any, index) => (
            <div className="w-full px-4">
              <div
                className={cn(
                  styles.selectItem,
                  data.length - 1 == index && '!border-b-0',
                )}
              >
                {iconMenu && (
                  <div className={styles.profileBorder}>
                    <Image
                      src={item?.img ?? iconMenu}
                      alt="profile icon"
                      style={{ objectFit: 'cover' }}
                      width={iconSize || 25}
                      height={iconSize || 25}
                    />
                  </div>
                )}
                <SelectItem
                  className={cn(
                    'text-body-medium text-neutral-900',
                    !iconMenu ? 'ps-0' : 'ps-2',
                  )}
                  value={item.value}
                >
                  {item.label}
                </SelectItem>
              </div>
            </div>
          ))}
        </SelectContent>
      </Select>

      {variant === 'border_bottom' && <Divider />}
    </>
  );
};

export default CSelect;
