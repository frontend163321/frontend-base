'use client';

import { cn } from '@/lib/utils';
import { useRouter } from 'next/navigation';

const MenuBar = ({
  data,
  active,
  notification,
}: {
  data: any;
  active: string;
  notification?: boolean;
}) => {
  const { push } = useRouter();

  const generatePath = (item: string) =>
    item.split(' ').reverse().join('-').toLowerCase();

  console.log(data);

  return (
    <div
      className={cn(
        'w-full grid grid-cols-7 p-1 rounded-full bg-neutral-200',
        data.pushPath === '/payment' ? 'grid-cols-3' : 'grid-cols-7',
      )}
    >
      {data?.name?.map((item: any) => (
        <button
          className={cn(
            'relative rounded-full flex w-full justify-center items-center text-label-medium h-10',
            active === generatePath(item)
              ? 'bg-secondary-500 text-neutral-100'
              : 'bg-transparent text-neutral-700',
          )}
          onClick={() => push(`${data?.pushPath}/${generatePath(item)}`)}
        >
          {item}
          {notification && (
            <div className="absolute right-3 h-5 w-5 rounded-full flex justify-center items-center bg-primary text-label-xsmall shadow-sm text-neutral-900">
              99
            </div>
          )}
        </button>
      ))}
    </div>
  );
};

export default MenuBar;
