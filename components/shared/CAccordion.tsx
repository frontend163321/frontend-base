import React from 'react';
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from '@/components/ui/accordion';
import { cn } from '@/lib/utils';

const CAccordion = ({
  labelColor,
  labelTitle,
  children,
  className,
}: CAccordion) => {
  return (
    <Accordion type="single" collapsible className="w-full">
      <AccordionItem className={cn('border-none')} value="item-1">
        <AccordionTrigger
          className={cn(
            'hover:no-underline !text-label-xxsmall',
            className,
            labelColor,
          )}
        >
          {labelTitle}
        </AccordionTrigger>
        <AccordionContent>{children}</AccordionContent>
      </AccordionItem>
    </Accordion>
  );
};

export default CAccordion;
