import Image from 'next/image';
import { useState } from 'react';

const CImage = ({
  src,
  alt,
  width,
  height,
  className,
  style,
  fill,
  roundedPlaceholder,
}: Image) => {
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const link = !isError ? src : '/images/photos/default-image.png';
  const props = fill ? { fill } : {};

  return (
    <div
      className={`${roundedPlaceholder ? roundedPlaceholder : 'rounded-none'} duration-700 ease-in-out relative h-full w-full overflow-hidden ${
        isLoading ? 'wave-animation bg-[#cbd5e0]' : 'bg-transparent'
      }`}
    >
      <Image
        className={`object-cover duration-700 ease-in-out object-top ${className} ${
          isLoading ? 'scale-125 blur-xl' : 'scale-100 blur-0'
        }`}
        src={link}
        alt={alt}
        width={width}
        height={height}
        style={style}
        placeholder="empty"
        onError={() => setIsError(true)}
        onLoadingComplete={() => setIsLoading(false)}
        priority
        {...props}
      />
    </div>
  );
};

export default CImage;
