import {
  InputOTP,
  InputOTPGroup,
  InputOTPSlot,
} from '@/components/ui/input-otp';
import styles from '@styles/components/inputPin.module.css';

const InputOtp = ({ value }: { value: string }) => {
  return (
    <InputOTP maxLength={6} value={value}>
      <InputOTPGroup className="w-full gap-4 mt-2">
        <InputOTPSlot className={styles.pinItem} index={0} />
        <InputOTPSlot className={styles.pinItem} index={1} />
        <InputOTPSlot className={styles.pinItem} index={2} />
        <InputOTPSlot className={styles.pinItem} index={3} />
        <InputOTPSlot className={styles.pinItem} index={4} />
        <InputOTPSlot className={styles.pinItem} index={5} />
      </InputOTPGroup>
    </InputOTP>
  );
};

export default InputOtp;
