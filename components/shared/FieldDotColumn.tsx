import { DotsIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import Image from 'next/image';
import React from 'react';

const FieldDotColumn = ({
  label,
  value,
  valueEnd,
  className,
  classNameValue,
  color = 'text-neutral-600',
  fontSize = 'text-label-xxsmall',
  fontWeightEnd,
  colorEnd,
}: FieldDotColumn) => {
  return (
    <div className={cn('flex gap-2 items-start mb-[6px]', className)}>
      <div className="flex gap-2 flex-shrink-0">
        <label className={cn(`!${fontSize}`, color)}>{label}</label>
        <Image src={DotsIcon} alt="dots icon" />
      </div>

      <div className="flex w-full justify-between gap-5">
        <label
          className={cn('text-wrap', `!${fontSize}`, classNameValue, color)}
        >
          {value}
        </label>
        {valueEnd && (
          <span
            className={cn(
              'text-neutral-700 flex-shrink-0',
              `!${fontWeightEnd}`,
              `!${colorEnd}`,
              `!${fontSize}`,
            )}
          >
            {valueEnd}
          </span>
        )}
      </div>
    </div>
  );
};

export default FieldDotColumn;
