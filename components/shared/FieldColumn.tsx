import { cn } from '@/lib/utils';

const FieldColumn = ({
  width,
  label,
  value,
  between,
  size = 'default',
  valueColor,
}: FieldColumn) => {
  return (
    <div
      className={cn(
        'w-full flex items-start',
        between ? 'justify-between' : '',
      )}
    >
      <div className={cn('flex-shrink-0', !between && (width || 'w-[23%]'))}>
        <label
          className={cn(
            'text-neutral-900',
            size === 'medium' ? 'text-body-small' : 'text-body-xsmall',
          )}
        >
          {label}
        </label>
      </div>

      <div className="flex gap-2">
        {!between && <span>:</span>}
        <div
          className={cn(
            'text-neutral-900 line-clamp-1 mt-[3px] !text-label-small',
            valueColor,
          )}
        >
          {value}
        </div>
      </div>
    </div>
  );
};

export default FieldColumn;
