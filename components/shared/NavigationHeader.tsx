'use client';

import { BackIcon, WhitePlusIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import CButton from '@components/button/CButton';
import LayoutMain from '@components/layout/LayoutMain';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import { Button } from '../ui/button';

const NavigationHeader = ({ title, path }: { title: string; path: string }) => {
  const { back, push } = useRouter();

  const endButton = path === '/list-order';

  return (
    <div className="h-20 shadow-bottom relative !z-30">
      <LayoutMain className={cn(endButton && 'flex justify-between')}>
        <div className="flex items-center gap-4">
          <CButton
            icon={BackIcon}
            iconSize={24}
            className="!p-2"
            onClick={() => back()}
          />
          <h1 className="text-heading-4 text-black">{title}</h1>
        </div>

        {endButton && (
          <Button
            className="bg-success-500 text-button-large text-white font-semibold gap-2"
            onClick={() => push('/')}
          >
            <Image src={WhitePlusIcon} alt="add icon" />
            Tambah Pesanan
          </Button>
        )}
      </LayoutMain>
    </div>
  );
};

export default NavigationHeader;
