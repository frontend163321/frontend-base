import { BackspaceIcon } from '@/lib/images';
import styles from '@styles/components/numpad.module.css';
import Image from 'next/image';
import InputOtp from './InputOtp';

const Numpad = ({
  setValue,
  value,
}: {
  setValue: (value: string) => void;
  value: string;
}) => {
  const roundedNumpad = (index: number) => {
    switch (index + 1) {
      case 1:
        return 'rounded-tl-base';
      case 3:
        return 'rounded-tr-base';
      case 10:
        return 'rounded-bl-base';
      case 12:
        return 'rounded-br-base';
      default:
        return '';
    }
  };

  const borderNumpad = (index: number) => {
    const isBottomRow = index >= 12;
    const isRightColumn = (index + 1) % 13 === 0;

    let classes = 'border-neutral-300';

    if (!isBottomRow) {
      classes += ' border-b';
    }
    if (!isRightColumn) {
      classes += ' border-r';
    }

    if (index < 3) {
      classes += ' border-t';
    }
    if (index % 3 === 0) {
      classes += ' border-l';
    }

    return classes;
  };

  const numpadItem = (index: number) => {
    switch (index + 1) {
      case 10:
        return <div className={styles.clear}>C</div>;
      case 11:
        return <div className={styles.number}>{0}</div>;
      case 12:
        return <Image src={BackspaceIcon} alt="backpack icon" width={25} />;
      default:
        return <div className={styles.number}>{index + 1}</div>;
    }
  };

  const handleChangeNumpad = (index: string) => {
    if (index === '12') {
      // Handle delete operation
      setValue(value.slice(0, -1));
    } else if (index === '11') {
      // Handle setting value to 0
      setValue('0');
    } else if (index === '10') {
      // Handle clear and set value to 0
      setValue('');
    } else {
      // Handle insert number
      value.length < 6 && setValue(value + index);
    }
  };

  return (
    <>
      <InputOtp value={value} />
      <div className={styles.numpad}>
        {Array.from({ length: 12 }).map((_, index) => {
          const _rounded = roundedNumpad(index);
          const _border = borderNumpad(index);

          return (
            <div
              key={index}
              className={`${styles.numpadItem} ${_rounded} ${_border}`}
              onClick={() => handleChangeNumpad(String(index + 1))}
            >
              {numpadItem(index)}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Numpad;
