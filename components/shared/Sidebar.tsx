import { Sheet, SheetContent } from '@/components/ui/sheet';
import { sidebar } from '@/constants';
import {
  BgPatternSidebar,
  BgSidebar,
  LogoutIcon,
  OrderiaBrand,
} from '@/lib/images';
import Image from 'next/image';
import { useState } from 'react';
import CButton from '../button/CButton';

const Sidebar = ({ open, setIsOpen, side = 'left' }: Sidebar) => {
  const [active, setActive] = useState<string>(sidebar[0].label);

  return (
    <Sheet open={open} onOpenChange={setIsOpen}>
      <SheetContent
        className="flex flex-col justify-between w-[240px] p-0"
        side={side}
      >
        <div>
          <div className="relative h-[122px] w-full bg-[#FFFAEC] rounded-b-base overflow-hidden">
            <Image
              src={BgSidebar}
              alt="bg sidebar"
              className="absolute bottom-0 right-[-15px]"
            />
            <Image
              src={BgPatternSidebar}
              alt="bg sidebar"
              className="absolute opacity-30"
              fill
            />

            <div className="p-5 relative z-10">
              {/* profile */}
              <div className="flex items-center gap-2 mb-4">
                <div>
                  <Image
                    src={'/images/photos/me.png'}
                    alt="profile"
                    width={50}
                    height={50}
                    className="rounded-full"
                  />
                </div>

                <div>
                  <label className="text-label-xsmall text-neutral-600">
                    Cashier
                  </label>
                  <h3 className="text-body-medium text-neutral-900">
                    Rio Prayoga Teja
                  </h3>
                </div>
              </div>

              {/* resto name */}
              <h4 className="text-heading-8 text-secondary">
                Gubuk Udang Resto Bandung
              </h4>
            </div>
          </div>

          <div className="p-5">
            {sidebar.map(item => (
              <div
                className={`cursor-pointer flex gap-3 h-11 items-center p-3 ${active === item.label ? 'bg-neutral-200 text-label-medium' : ''} rounded - base}`}
                onClick={() => setActive(item.label)}
              >
                <Image src={item.icon} alt={item.label} />
                <span>{item.label}</span>
              </div>
            ))}
          </div>
        </div>

        <div>
          <div className="p-5">
            <CButton
              variant="outline"
              className="border-secondary bg-neutral-100 p-3 w-full text-body-medium text-secondary hover:text-secondary gap-3 justify-start"
            >
              <Image src={LogoutIcon} alt="menu icon" width={20} height={20} />
              Logout
            </CButton>
          </div>

          <div className="flex flex-col items-center justify-center bg-primary-50 h-[76px] border-t border-neutral-300">
            <Image src={OrderiaBrand} width={75} height={26} alt="orderia" />
            <label className="text-label-xxsmall text-neutral-600 mt-1">
              Version 01.02.1
            </label>
          </div>
        </div>
      </SheetContent>
    </Sheet>
  );
};

export default Sidebar;
