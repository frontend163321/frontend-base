import { ErrorHero } from '@/lib/images';
import Image from 'next/image';
import React from 'react';

const ErrorContent = () => {
  return (
    <div className="flex flex-col justify-center items-center py-[37px]">
      <Image src={ErrorHero} alt="error hero" />
      <h4 className="flex flex-col items-center text-heading-4 text-neutral-900 mt-[36px]">
        <span>Uppps!!!</span> Pembatalan Tidak Dapat Dilakukan
      </h4>
      <div className="w-[536px]">
        <p className="mt-3 text-body-medium text-center text-neutral-900">
          Pesanan ini sudah dibayar dan tidak dapat dibatalkan dari halaman
          daftar pesanan yang sedang berjalan. Harap selesaikan pesanan terlebih
          dahulu. Setelah pesanan selesai, Anda dapat mengakses fitur receipt
          untuk memproses refund jika diperlukan.
        </p>
      </div>
    </div>
  );
};

export default ErrorContent;
