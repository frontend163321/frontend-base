import { CloseIcon } from '@/lib/images';
import { setCloseModal } from '@/store/modal/action';
import Image from 'next/image';
import React from 'react';
import { useDispatch } from 'react-redux';

const CloseIconModal = () => {
  const dispatch = useDispatch();

  return (
    <Image
      src={CloseIcon}
      alt="close icon"
      priority
      className="cursor-pointer"
      onClick={() => dispatch(setCloseModal())}
    />
  );
};

export default CloseIconModal;
