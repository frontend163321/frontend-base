import React from 'react';
import CButton from '../button/CButton';
import { MinusIcon, PlusIcon } from '@/lib/images';
import { cn } from '@/lib/utils';

const CountTotal = ({ type = 'payment' }: { type?: 'payment' | 'order' }) => {
  return (
    <div className={cn('mb-5 flex gap-2')}>
      <CButton
        icon={MinusIcon}
        iconSize={24}
        className={cn(
          'flex justify-center rounded-[4px]',
          type === 'order' && 'w-10 h-10',
        )}
        size={type === 'order' ? 'default' : 'large'}
      />
      <div
        className={cn(
          'flex justify-center items-center text-body-large rounded-[4px] bg-neutral-200',
          type === 'order' ? 'w-[60px]' : 'w-[107px]',
        )}
      >
        1
      </div>
      <CButton
        icon={PlusIcon}
        iconSize={24}
        className={cn(
          'flex justify-center rounded-[4px]',
          type === 'order' && 'w-10 h-10',
        )}
        size={type === 'order' ? 'default' : 'large'}
      />
    </div>
  );
};

export default CountTotal;
