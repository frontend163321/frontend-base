import { StarsIcon } from '@/lib/images';
import styles from '@styles/components/card.module.css';
import Image from 'next/image';

const LabelStars = () => {
  return (
    <div className={styles.labelStars}>
      <Image src={StarsIcon} alt="stars icon" />
    </div>
  );
};

export default LabelStars;
