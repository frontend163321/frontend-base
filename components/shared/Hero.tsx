import styles from '@styles/components/hero.module.css';
import Image from 'next/image';

const Hero = ({ src, heading, description, wrapText }: OnBoarding) => {
  return (
    <>
      <Image
        src={src}
        className={`${styles.imageOnboarding}`}
        alt="oboarding hero"
        priority
      />

      <h1
        className={`${wrapText ? '' : styles.content} text-center text-heading-2 mb-[16px] h-[88px]`}
      >
        {heading}
      </h1>

      <p
        className={`${wrapText ? '' : styles.content} text-center text-body-large`}
      >
        {description}
      </p>
    </>
  );
};

export default Hero;
