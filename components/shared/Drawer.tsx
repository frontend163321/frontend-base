import { Button } from '@/components/ui/button';
import { Drawer, DrawerContent, DrawerTrigger } from '@/components/ui/drawer';

export const CDrawer = ({ open = false }: { open: boolean }) => {
  return (
    <Drawer open={open}>
      <DrawerTrigger>
        <Button variant="outline">Open Drawer</Button>
      </DrawerTrigger>
      <DrawerContent className="min-h-40 max-h-64"></DrawerContent>
    </Drawer>
  );
};
