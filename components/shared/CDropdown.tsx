import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from '@/components/ui/dropdown-menu';

const CDropdown = ({ triggerButton, menuContent, width }: CDropdown) => {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger>{triggerButton}</DropdownMenuTrigger>
      <DropdownMenuContent className={width}>{menuContent}</DropdownMenuContent>
    </DropdownMenu>
  );
};

export default CDropdown;
