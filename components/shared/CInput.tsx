'use client';

import { Input } from '@/components/ui/input';
import styles from '@styles/components/cinput.module.css';
import Image from 'next/image';
import Divider from './Divider';

const CInput = ({
  borderBottom,
  type,
  placeholder,
  field,
  className,
  classNameParent,
  dir = 'ltr',
  inputMode = 'text',
  value,
  onChange,
  inputRef,
  onSelect,
  size = 'medium',
  icon,
  iconSize,
  children,
  defaultValue,
  required,
  checked,
}: CInput) => {
  const _size = size === 'big' ? 'h-[48px]' : 'h-[40px]';

  return (
    <>
      <div
        className={`flex items-center gap-2 ${styles.cinput} ${classNameParent} ${(type === 'radio' || type === 'checkbox' || borderBottom) && '!bg-transparent !ps-0'}`}
      >
        <>
          {children}
          {icon && (
            <Image
              src={icon}
              alt="profile icon"
              width={iconSize || 25}
              height={iconSize || 25}
              className="flex-shrink-0"
            />
          )}
          <Input
            ref={inputRef}
            className={`${className || ''} ${type === 'radio' ? '' : ''} ${borderBottom ? 'border-none px-0' : `bg-neutral-200 ${children ? 'ps-2' : 'ps-0'}`} ${_size}`}
            value={value}
            defaultValue={defaultValue}
            type={type || 'text'}
            placeholder={`${placeholder} ${required ? '*' : ''}`}
            onWheel={event => event.currentTarget.blur()}
            dir={dir}
            inputMode={inputMode}
            onSelect={onSelect}
            onChange={onChange}
            checked={checked}
            {...field}
          />
        </>
      </div>
      {borderBottom && <Divider className="!m-0 !p-0" />}
    </>
  );
};

export default CInput;
