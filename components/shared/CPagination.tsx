import Image from 'next/image';
import { Button } from '../ui/button';
import { ChevronLeftIcon, ChevronRightIcon } from '@/lib/images';

const CPagination = () => {
  return (
    <div className="w-full flex gap-2 justify-center">
      <Button
        variant={'ghost'}
        className="bg-neutral-200 h-10 w-10 border-neutral-200"
      >
        <Image src={ChevronLeftIcon} alt="asd" />
      </Button>
      {Array.from({ length: 5 }).map((_, index) => (
        <Button
          variant={'ghost'}
          className="bg-neutral-200 h-10 w-10 border border-primary"
        >
          {index + 1}
        </Button>
      ))}
      <Button
        variant={'ghost'}
        className="bg-neutral-200 h-10 w-10 border-neutral-200"
      >
        <Image src={ChevronRightIcon} alt="asd" />
      </Button>
    </div>
  );
};

export default CPagination;
