import { cn } from '@/lib/utils';
import React from 'react';

const Divider = ({
  color,
  className,
  type = 'default',
}: {
  color?: string;
  className?: string;
  type?: 'default' | 'dashed';
}) => {
  return (
    <div
      className={cn(
        'border-b-[1px]',
        color || 'border-neutral-500',
        type === 'dashed' && 'border-dashed',
        className,
      )}
    />
  );
};

export default Divider;
