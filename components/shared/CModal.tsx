import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import styles from '@styles/components/cmodal.module.css';
import ButtonBottom from '../button/ButtonBottom';
import CloseIconModal from './CloseIconModal';
import { cn } from '@/lib/utils';

const CModal = ({
  open,
  setIsOpen,
  height,
  width,
  children,
  shadowFooter = true,
  showFooter = true,
  showHeader = true,
  footerTitle,
  headerTitle,
  onClickFooter,
  paddingContent = true,
}: CModal) => {
  return (
    <Dialog open={open} onOpenChange={setIsOpen}>
      <DialogContent
        className={`rounded-lg ${styles.modalWrapContent} ${height} ${`${width}`}`}
      >
        {showHeader && (
          <DialogHeader className={styles.modalHeader}>
            <div>
              <DialogTitle className={styles.modalDialogTitle}>
                {headerTitle}
              </DialogTitle>

              <CloseIconModal />
            </div>
          </DialogHeader>
        )}

        <div className={cn(styles.modalContent, paddingContent ? 'p-4' : '')}>
          {children}
        </div>

        {showFooter && (
          <ButtonBottom
            onClick={onClickFooter}
            title={footerTitle}
            shadow={shadowFooter}
          />
        )}
      </DialogContent>
    </Dialog>
  );
};

export default CModal;
