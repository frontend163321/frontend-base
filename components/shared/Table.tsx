import {
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import CPagination from './CPagination';

const Ctable = ({
  headerData,
  children,
  withNumber,
  withAction = true,
}: CTable) => {
  const collspanPlus = withAction && withNumber ? 2 : withAction ? 1 : 0;

  return (
    <div className="border border-neutral-300 rounded-base overflow-hidden">
      <Table>
        <TableHeader className="bg-warning-100">
          <TableRow className="!h-[40px]">
            {withNumber && (
              <TableHead className="w-[30px] !text-heading-7 text-neutral-900">
                No
              </TableHead>
            )}
            {headerData.map((fieldName, index) => (
              <TableHead className="!text-heading-7 text-neutral-900">
                {fieldName}
              </TableHead>
            ))}
            <TableHead className="!text-heading-7 text-neutral-900">
              Aksi
            </TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>{children}</TableBody>

        <TableFooter>
          <TableCell colSpan={headerData.length + collspanPlus}>
            <CPagination />
          </TableCell>
        </TableFooter>
      </Table>
    </div>
  );
};

export default Ctable;
