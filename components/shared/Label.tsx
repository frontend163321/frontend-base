import { PromoLabel } from '@/lib/images';
import styles from '@styles/components/card.module.css';
import Image from 'next/image';

const Label = () => {
  return (
    <div className={styles.label}>
      <div>
        <Image src={PromoLabel} alt="promo label" />
        <label>Promo</label>
      </div>
    </div>
  );
};

export default Label;
