'use client';

import React from 'react';
import CImage from '../shared/CImage';
import styles from '@styles/modules/cash.module.css';

const Profile = () => {
  return (
    <>
      <div className={styles.profile}>
        <CImage src="/images/photos/me.png" alt="profile image" fill />
      </div>

      <h1 className={`text-heading-5 ${styles.title}`}>Hallo, Rio Prayoga!</h1>
      <p className="text-body-medium text-neutral-100 mt-3">
        Apakah kamu sudah siap menerima pesanan?
      </p>
    </>
  );
};

export default Profile;
