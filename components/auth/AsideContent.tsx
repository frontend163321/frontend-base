import React from 'react';
import styles from '@styles/modules/auth.module.css';

const AsideContent = ({ children }: { children: React.ReactNode }) => {
  return <aside className={styles.aside}>{children}</aside>;
};

export default AsideContent;
