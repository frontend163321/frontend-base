'use client';

import { BgCash, BgLogin, OrderiaBrand } from '@/lib/images';
import styles from '@styles/modules/auth.module.css';
import Image from 'next/image';
import { usePathname } from 'next/navigation';
import React from 'react';

const MainContent = ({
  children,
  brandLogo,
}: {
  children: React.ReactNode;
  brandLogo?: boolean;
}) => {
  const pathname = usePathname();

  return (
    <div
      className={`${styles.mainContent} ${pathname === '/cash' && 'bg-secondary'}`}
    >
      {/* orderia brand */}
      {brandLogo && (
        <Image
          src={OrderiaBrand}
          alt="orderia brand logo"
          className="absolute z-10 top-5 left-5"
        />
      )}

      {/* background image */}
      <Image
        className="w-full h-full"
        src={pathname === '/cash' ? BgCash : BgLogin}
        alt="background image login"
        fill
      />

      <div className={styles.content}>{children}</div>
    </div>
  );
};

export default MainContent;
