import FilterCashier from '@/components/dashboard-cashier/Filter';
import Menu from '@components/onboarding/Menu';
import HeaderCashier from '@components/header/HeaderCashier';

const MainContent = () => {
  return (
    <>
      <HeaderCashier />
      <FilterCashier />
      <Menu />
    </>
  );
};

export default MainContent;
