import { NoteIcon } from '@/lib/images';
import { cn } from '@/lib/utils';
import Image from 'next/image';
import FieldDotColumn from '../shared/FieldDotColumn';

const AccordionMenuDetail = ({
  type = 'payment',
}: {
  type?: 'payment' | 'order';
}) => {
  return (
    <div
      className={cn('gap-2 items-center', type === 'payment' ? 'mt-4' : 'mt-1')}
    >
      <FieldDotColumn
        label="Category"
        value="Test1, test2, test,3"
        valueEnd="123.000"
        fontSize={type === 'order' ? 'text-label-xsmall' : 'text-label-xxsmall'}
        fontWeightEnd={type === 'order' ? 'font-semibold' : ''}
        colorEnd={type === 'order' ? 'text-neutral-700' : ''}
      />
      <FieldDotColumn
        label="Category"
        value="Test1, test2, test,3"
        valueEnd="123.000"
        fontSize={type === 'order' ? 'text-label-xsmall' : 'text-label-xxsmall'}
        fontWeightEnd={type === 'order' ? 'font-semibold' : ''}
        colorEnd={type === 'order' ? 'text-neutral-700' : ''}
      />
      <FieldDotColumn
        label="Category"
        value="Test1, test2, test,3"
        valueEnd="123.000"
        fontSize={type === 'order' ? 'text-label-xsmall' : 'text-label-xxsmall'}
        fontWeightEnd={type === 'order' ? 'font-semibold' : ''}
        colorEnd={type === 'order' ? 'text-neutral-700' : ''}
      />

      <div className="flex items-center gap-1">
        <Image
          src={NoteIcon}
          className={cn(type === 'order' ? 'w-4 h-4' : '')}
          alt="note icon"
        />
        <p
          className={cn(
            'text-neutral-700',
            type === 'payment' ? 'text-body-xsmall' : 'text-body-small',
          )}
        >
          Medium well
        </p>
      </div>
    </div>
  );
};

export default AccordionMenuDetail;
