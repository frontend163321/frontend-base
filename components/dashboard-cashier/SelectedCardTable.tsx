import { WhiteCloseIcon } from '@/lib/images';
import Image from 'next/image';

const SelectedCardTable = () => {
  return (
    <div className="relative h-[39px] px-3 w-fit border bg-success-50 border-success-500 text-success-600 rounded-sm flex items-center flex-shrink-0 text-label-medium">
      Meja A300
      <div className="bg-error-500 flex justify-center rounded-full w-6 h-6 absolute top-[-10px] right-[-10px]">
        <Image src={WhiteCloseIcon} alt="close icon" />
      </div>
    </div>
  );
};

export default SelectedCardTable;
