'use client';

import React, { useState } from 'react';
import styles from '@styles/modules/dashboard.module.css';
import { filterDashboard } from '@/constants';
import ButtonFilter from '../button/ButtonFilter';

const FilterCashier = () => {
  const [active, setActive] = useState<string>(filterDashboard[0]);

  return (
    <div className={styles.filter}>
      <div className={styles.filterBlur} />

      <div className={`${styles.filterContent} pe-4`}>
        {filterDashboard.map((title, index) => (
          <ButtonFilter
            title={title}
            first={index === 0}
            onClick={() => setActive(title)}
            active={active === title}
          />
        ))}
      </div>
    </div>
  );
};

export default FilterCashier;
