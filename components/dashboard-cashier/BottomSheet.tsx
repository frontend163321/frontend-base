import { DividerLine } from '@/lib/images';
import Image from 'next/image';
import LayoutMain from '../layout/LayoutMain';
import { Button } from '../ui/button';
import SelectedCardTable from './SelectedCardTable';

const BottomSheet = () => {
  return (
    <LayoutMain className="absolute bottom-52 w-full pb-0">
      <div className="w-full bg-neutral-100 border border-white z-10 h-[72px] px-5 flex items-center shadow-lg rounded-base">
        <div className="w-full gap-3 flex items-center">
          <div className="flex items-center gap-4 flex-shrink-0">
            <label className="text-body-small text-neutral-700">
              No Pemesanan
            </label>
            <span className="text-label-medium text-neutral-900">
              ORD-20240403-49790
            </span>
            <Image src={DividerLine} alt="divider" />
            <label className="text-body-small text-neutral-700">
              Meja Terpilih
            </label>
          </div>

          <div className="flex gap-4 flex-1">
            {Array.from({ length: 3 }).map((_, index) => (
              <SelectedCardTable />
            ))}
          </div>

          <Button className="h-[48px] w-[98px] !text-button-large">
            Simpan
          </Button>
        </div>
      </div>
    </LayoutMain>
  );
};

export default BottomSheet;
