import { DocumentIcon } from '@/lib/images';
import styles from '@styles/modules/dashboard.module.css';
import dynamic from 'next/dynamic';
import Image from 'next/image';
import CButton from '../button/CButton';
import CashierField from './CashierField';

const CardMenuDetail = dynamic(
  () => import('@components/cards/CardMenuDetail'),
  { ssr: false },
);

const AsideContent = () => {
  return (
    <>
      <div className="p-5">
        <div className={styles.contentHeader}>
          <div>
            <h3 className="text-label-medium mb-1">ORD-20240403-49790</h3>
            <p className="text-body-xsmall text-neutral-600">
              16 Mei 2023 - 10:15
            </p>
          </div>

          <CButton type="filled" className="gap-2" size="large">
            <Image src={DocumentIcon} alt="document icon" />
            <span className="text-neutral-500 font-semibold">Simpan</span>
          </CButton>
        </div>
        <CashierField />
      </div>

      <div className={styles.contentBody}>
        <div className={styles.contentBodyHeader}>
          <h4 className="text-heading-7 text-neutral-900">Detail Pemesanan</h4>
          <div className="text-button-medium !text-error-300">Hapus</div>
        </div>

        <div className="h-full overflow-y-auto gap-2 mt-3 flex flex-col pb-[30rem]">
          {Array.from({ length: 15 }).map((_, index) => (
            <CardMenuDetail />
          ))}
        </div>
      </div>
    </>
  );
};

export default AsideContent;
