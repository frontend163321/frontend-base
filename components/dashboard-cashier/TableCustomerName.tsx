import { TableCell, TableHead } from '@/components/ui/table';
import { headerCustomerName } from '@/constants';
import { useState } from 'react';

const users = [
  {
    name: 'name1',
    noHp: '085678978969',
    email: 'email@gmail.com Card',
  },
  {
    name: 'name2',
    noHp: '085678978969',
    email: 'email@gmail.com',
  },
  {
    name: 'name3',
    noHp: '085678978969',
    email: 'email@gmail.com Transfer',
  },
  {
    name: 'name4',
    noHp: '085678978969',
    email: 'email@gmail.com Card',
  },
  {
    name: 'name5',
    noHp: '085678978969',
    email: 'email@gmail.com',
  },
  {
    name: 'name6',
    noHp: '085678978969',
    email: 'email@gmail.com Transfer',
  },
  {
    name: 'name7',
    noHp: '085678978969',
    email: 'email@gmail.com Card',
  },
  {
    name: 'name7',
    noHp: '085678978969',
    email: 'email@gmail.com Card',
  },
  {
    name: 'name7',
    noHp: '085678978969',
    email: 'email@gmail.com Card',
  },
];

const TableCustomerName = ({ className }: { className?: string }) => {
  const [activeRow, setActiveRow] = useState<number>(0);
  const RowCell = ({ value }: { value: string }) => {
    return (
      <TableCell className="px-4 !text-black text-body-small h-[44px] flex items-center">
        {value}
      </TableCell>
    );
  };

  return (
    <div
      className={`border border-neutral-300 rounded-base overflow-hidden h-[255px] ${className}`}
    >
      <div className="grid grid-cols-3">
        {headerCustomerName.map(fieldName => (
          <TableHead className="px-4 bg-neutral-300 !text-black text-label-small flex items-center">
            {fieldName}
          </TableHead>
        ))}
      </div>

      <div className="!h-[220px] pb-3 overflow-y-auto">
        {users.map((users, index) => (
          <div
            className={`grid grid-cols-3 cursor-pointer ${activeRow === index + 1 && '!bg-neutral-200'}`}
            onClick={() => setActiveRow(index + 1)}
          >
            <RowCell value={users.name} />
            <RowCell value={users.email} />
            <RowCell value={users.noHp} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default TableCustomerName;
