import React from 'react';
import CInput from '../shared/CInput';
import { SearchIcon } from '@/lib/images';
import CSelect from '../shared/CSelect';
import { roomFilter, tableFilterCategory } from '@/constants';
import TableStatus from '../onboarding/TableStatus';

const HeaderFilterTable = ({
  active,
  setActive,
}: {
  active: string;
  setActive: (active: string) => void;
}) => {
  return (
    <div className="grid grid-cols-2 gap-6">
      <div className="flex gap-2">
        <CInput
          icon={SearchIcon}
          iconSize={20}
          placeholder="Cari Menu"
          size="medium"
          classNameParent="!w-[257px]"
        />
        <CSelect
          data={roomFilter}
          placeholder={roomFilter[0].label}
          size="medium"
          className="!w-[257px]"
        />
      </div>

      <div className="flex items-center gap-2 overflow-y-auto pe-5">
        {tableFilterCategory.map(label => (
          <TableStatus label={label} active={active} setActive={setActive} />
        ))}
      </div>
    </div>
  );
};

export default HeaderFilterTable;
