'use client';

import { reservationType as reservationTypeData } from '@/constants';
import {
  ChevronRightIcon,
  PeopleIcon,
  ProfileAddIcon,
  TableIcon,
} from '@/lib/images';
import { setOpenModal } from '@/store/modal/action';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import CButton from '../button/CButton';
import CSelect from '../shared/CSelect';
import FieldColumn from '../shared/FieldColumn';
import { Button } from '../ui/button';

const CashierField = () => {
  const [reservationType, setReservationType] = useState(
    reservationTypeData[0].value,
  );

  const dispatch = useDispatch();
  const { push } = useRouter();

  const handleClickModal = ({
    modalName,
    headerTitle,
    footerTitle,
    width,
    height,
    shadowFooter = true,
  }: HandleClickModal) => {
    dispatch(
      setOpenModal({
        modalName,
        headerTitle,
        footerTitle,
        height,
        width,
        shadowFooter,
      }),
    );
  };

  return (
    <>
      <CSelect
        className="mb-3"
        placeholder="Dine In"
        data={reservationTypeData}
        icon={false}
        iconMenu={false}
        variant="secondary"
        placeholderSize="small"
        center
        onChange={setReservationType}
      />

      {reservationType === 'dine-in' && (
        <CButton
          className="w-full justify-start mb-2"
          title="Pilih Meja"
          icon={TableIcon}
          rightIcon={ChevronRightIcon}
          type="filled"
          fontSize="small"
          fontWeight="normal"
          iconSize={20}
          iconSizeRight={10}
          onClick={() => push('/table')}
        />
      )}

      <div className="flex gap-2">
        {reservationType !== 'reservation' && (
          <CButton
            className="w-full justify-start"
            title="Nama Pelanggan"
            icon={ProfileAddIcon}
            type="filled"
            fontSize="small"
            fontWeight="normal"
            iconSize={20}
            onClick={() =>
              handleClickModal({
                modalName: 'ModalCustomerName',
                headerTitle: 'Daftar Nama Pelanggang',
                footerTitle: 'Tambah Pelanggan Baru',
                height: 'h-[503px]',
                width: 'w-[784px]',
                shadowFooter: false,
              })
            }
          />
        )}

        {reservationType === 'dine-in' && (
          <CButton
            className="w-full justify-start"
            title="Jumlah Tamu"
            icon={PeopleIcon}
            type="filled"
            fontSize="small"
            fontWeight="normal"
            iconSize={20}
            onClick={() =>
              handleClickModal({
                modalName: 'guestTotalModal',
                headerTitle: 'Jumlah Tamu',
                height: 'h-[212px]',
                width: 'w-[412px]',
                shadowFooter: false,
                footerTitle: 'Simpan',
              })
            }
          />
        )}
      </div>

      {reservationType === 'reservation' && (
        <>
          <div className="flex flex-col gap-2 mb-2">
            <FieldColumn label="Nama Pelanggan" value="Rio Prayoga Teja" />
            <FieldColumn label="No. Hp" value="085795344094" />
            <FieldColumn label="Jumlah Tamu" value="12" />
            <FieldColumn label="Jadwal" value="19 Mei 2024" />
            <FieldColumn label="Jam" value="04:00 - 06:00 (2 Jam)" />
            <FieldColumn label="Acara" value="-" />
            <FieldColumn label="Catatan" value="-" />
          </div>
          <div className="flex justify-end">
            <Button
              variant={'ghost'}
              className="text-button-small text-info-500 p-0 m-0 h-fit"
              onClick={() =>
                handleClickModal({
                  modalName: 'editReservation',
                  headerTitle: 'Reservasi',
                  shadowFooter: false,
                  width: 'w-[592px]',
                  height: 'w-[584px]',
                })
              }
            >
              Ubah Data Reservasi
            </Button>
          </div>
        </>
      )}
    </>
  );
};

export default CashierField;
