import { EditIcon, TrashIcon } from '@/lib/images';
import CButton from '../button/CButton';

const DropdownActionMenu = () => {
  return (
    <div className="p-2 flex gap-2">
      <CButton
        className="!bg-success-500"
        icon={EditIcon}
        iconSize={24}
        size="small"
      />
      <CButton
        className="!bg-error-500"
        icon={TrashIcon}
        iconSize={24}
        size="small"
      />
    </div>
  );
};

export default DropdownActionMenu;
