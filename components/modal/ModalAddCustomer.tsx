import FormAddCustomer from '../forms/FormAddCustomer';

const ModalAddCustomer = () => {
  return (
    <>
      <FormAddCustomer />
    </>
  );
};

export default ModalAddCustomer;
