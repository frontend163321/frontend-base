import FormConfirmationArrival from '../forms/FormConfirmationArrival';

const ModalConfirmationArrival = () => {
  return (
    <>
      <p className="text-label-small text-neutral-900 mb-2">
        Masukkan Estimasi Persiapan Penghidangan
      </p>
      <FormConfirmationArrival />
    </>
  );
};

export default ModalConfirmationArrival;
