'use client';

import CModal from '@/components/shared/CModal';
import { IRootState } from '@/store';
import { setCloseModal, setOpenModal } from '@/store/modal/action';
import { useDispatch, useSelector } from 'react-redux';
import ModalAddCustomer from './ModalAddCustomer';
import ModalAddMenu from './ModalAddMenu';
import ModalCancelOrder from './ModalCancelOrder';
import ModalCustomerName from './ModalCustomerName';
import ModalEditReservation from './ModalEditReservation';
import ModalGuestTotal from './ModalGuestTotalModal';
import ModalSuccessPayment from './ModalSuccessPayment';
import ModalVerificationPin from './ModalVerificationPin';
import ModalSplitBill from './ModalSplitBill';
import ModalDetailReservation from './ModalDetailReservation';
import ModalConfirmationArrival from './ModalConfirmationArrival';

const CoreModal = () => {
  const {
    open,
    modalName,
    height,
    width,
    headerTitle,
    shadowFooter,
    footerTitle,
    showFooter,
    showHeader,
    paddingContent,
  } = useSelector((state: IRootState) => state.modal);
  const dispatch = useDispatch();

  const ModalContent = () => {
    switch (modalName) {
      case 'ModalAddMenu':
        return <ModalAddMenu />;
      case 'ModalCustomerName':
        return <ModalCustomerName />;
      case 'ModalAddCustomer':
        return <ModalAddCustomer />;
      case 'guestTotalModal':
        return <ModalGuestTotal />;
      case 'editReservation':
        return <ModalEditReservation />;
      case 'successPaymentModal':
        return <ModalSuccessPayment />;
      case 'cancelOrderModal':
        return <ModalCancelOrder />;
      case 'verificationPinModal':
        return <ModalVerificationPin />;
      case 'splitBillModal':
        return <ModalSplitBill />;
      case 'reservationDetailModal':
        return <ModalDetailReservation />;
      case 'confirmationArrivalModal':
        return <ModalConfirmationArrival />;
    }
  };

  const handleClickFooter = () => {
    switch (modalName) {
      case 'ModalCustomerName':
        return dispatch(
          setOpenModal({
            modalName: 'ModalAddCustomer',
            headerTitle: 'Tambah Pelanggan Baru',
            width: 'w-[412px]',
            height: 'h-[393px]',
            showFooter: false,
          }),
        );
      case 'cancelOrderModal':
        return dispatch(
          setOpenModal({
            modalName: 'verificationPinModal',
            headerTitle: 'Konfirmasi Kewenangan',
            width: 'w-[410px]',
            height: 'h-[614px]',
            showFooter: false,
          }),
        );

      default:
        null;
    }
  };

  return open ? (
    <CModal
      open={open}
      headerTitle={headerTitle}
      footerTitle={footerTitle}
      showFooter={showFooter}
      showHeader={showHeader}
      paddingContent={paddingContent}
      height={height}
      width={width}
      shadowFooter={shadowFooter}
      setIsOpen={() => dispatch(setCloseModal())}
      onClickFooter={handleClickFooter}
    >
      <ModalContent />
    </CModal>
  ) : null;
};

export default CoreModal;
