import { SearchIcon } from '@/lib/images';
import TableCustomerName from '../dashboard-cashier/TableCustomerName';
import CInput from '../shared/CInput';

const ModalCustomerName = () => {
  return (
    <>
      <CInput
        icon={SearchIcon}
        iconSize={20}
        placeholder="Cari Nama Pelanggan"
        size="big"
        classNameParent="!px-5 gap-1"
      />

      <div className="mt-6 h-full">
        <h6 className="text-heading-7 text-black">Baru Ditambahkan</h6>

        <TableCustomerName className="mt-[6px]" />
      </div>
    </>
  );
};

export default ModalCustomerName;
