'use client';

import { setOpenModal } from '@/store/modal/action';
import { useDispatch } from 'react-redux';
import { Button } from '../ui/button';

const ModalVerificationPin = () => {
  // const [pinValue] = useState<string[]>(['1', '2', '3', '4', '5', '6']);

  const dispatch = useDispatch();

  return (
    <>
      <label className="text-label-small text-neutral-900">
        Masukkan PIN untuk mengkonfirmasi
      </label>
      <h6 className="text-heading-6 text-neutral-900 mt-2 mb-[6px]">PIN</h6>
      {/* <Pin value={pinValue} gap={'!gap-[16.4px]'} /> */}
      {/* <Numpad /> */}
      <Button
        className="w-full mt-5"
        onClick={() =>
          dispatch(
            setOpenModal({
              paddingContent: false,
              modalName: 'successPaymentModal',
              width: 'w-[745px]',
              height: 'h-[697px]',
              showHeader: false,
              footerTitle: 'Simpan & Kembali ke Receipt',
            }),
          )
        }
      >
        Konfirmasi
      </Button>
    </>
  );
};

export default ModalVerificationPin;
