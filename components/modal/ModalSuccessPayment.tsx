import { PhoneIcon, PrinterIcon, SMSIcon, SendIcon } from '@/lib/images';
import IconSucces from '@components/animation/IconSucces';
import ButtonBottom from '@components/button/ButtonBottom';
import CButton from '@components/button/CButton';
import CardTicketReservation from '@components/dashboard-reservation/CardTicketReservation';
import CInput from '@components/shared/CInput';
import { Button } from '@components/ui/button';
import Image from 'next/image';

const ModalSuccessPayment = () => {
  return (
    <>
      <div className="p-4 h-full">
        <div className="flex flex-col items-center mt-1 gap-0">
          <IconSucces />
          <h3 className="text-heading-3 text-neutral-900">
            Pembayaran Berhasil
          </h3>
        </div>

        <CardTicketReservation />

        <div className="mt-5">
          <label className="text-label-large text-neutral-900">
            Pilih Cara Tanda Terima
          </label>

          <div className="mt-3 flex gap-3 items-end justify-between">
            <div className="w-full">
              <CInput
                icon={SMSIcon}
                placeholder="Email"
                className="text-base w-full"
                borderBottom
              />
            </div>

            <CButton
              icon={SendIcon}
              className="!h-[48px] !w-[109px] !bg-secondary-300 flex-shrink-0"
              fontColor="white"
              fontWeight="normal"
              title="Kirim"
            />
          </div>

          <div className="mt-3 flex gap-3 items-end justify-between">
            <div className="w-full">
              <CInput
                icon={PhoneIcon}
                placeholder="No. Hp"
                className="text-base w-full"
                borderBottom
              />
            </div>

            <CButton
              icon={SendIcon}
              className="!h-[48px] !w-[109px] !bg-secondary-300 flex-shrink-0"
              fontColor="white"
              fontWeight="normal"
              title="Kirim"
            />
          </div>

          <Button
            className="w-full gap-1 text-button-large text-neutral-100 mt-3"
            variant={'secondary'}
          >
            <Image src={PrinterIcon} alt="printer icon" />
            Print
          </Button>
        </div>
      </div>

      <ButtonBottom shadow>
        <Button
          className="w-full !text-button-large border-primary"
          variant={'outline'}
        >
          Simpan ke Daftar Pesanan
        </Button>
        <Button className="w-full !text-button-large" variant={'default'}>
          Simpan ke Daftar Pesanan
        </Button>
      </ButtonBottom>
    </>
  );
};

export default ModalSuccessPayment;
