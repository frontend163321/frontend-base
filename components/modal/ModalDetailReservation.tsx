import { useDispatch } from 'react-redux';
import CardMenuDetail from '../cards/CardMenuDetail';
import Divider from '../shared/Divider';
import FieldColumn from '../shared/FieldColumn';
import { Button } from '../ui/button';
import { setOpenModal } from '@/store/modal/action';

const ModalDetailReservation = () => {
  const dispatch = useDispatch();

  const handleClickConfirmation = () =>
    dispatch(
      setOpenModal({
        modalName: 'confirmationArrivalModal',
        width: 'w-[412px]',
        height: 'h-[232px]',
        showFooter: false,
        headerTitle: 'Konfirmasi Kedatangan',
      }),
    );

  return (
    <div className="p-1">
      <div className="flex justify-between">
        <label className="text-label-medium text-neutral-900">
          ORD-20240403-49790
        </label>
        <span className="text-label-medium text-secondary-500">
          Makan Ditempat
        </span>
      </div>

      <Divider color="border-neutral-300" className="my-2" />

      <div className="flex flex-col gap-1 mb-[42px]">
        <FieldColumn
          width="w-[14%]"
          label="Tanggal"
          value="Jumat, 17 Nov 2024"
        />
        <FieldColumn width="w-[14%]" label="Jam" value="13:00 - 14:00" />
        <FieldColumn
          width="w-[14%]"
          label="Nama Pemesan"
          value="Fikry N Zaman"
        />
        <FieldColumn width="w-[14%]" label="No. Hp" value="081234567890" />
        <FieldColumn width="w-[14%]" label="Jumlah Tamu" value="4" />
        <FieldColumn
          width="w-[14%]"
          label="Nama Area"
          value="Ruang Area Merokok Berasap Gold"
        />
        <FieldColumn
          width="w-[14%]"
          label="Meja"
          value="Meja A001, Meja A002"
        />
      </div>

      <h6 className="text-heading-7 text-neutral-900 mb-[6px]">Pesanan</h6>
      <div className="grid grid-cols-2 gap-6">
        <div className="overflow-y-auto flex flex-col gap-2 h-[224px]">
          <CardMenuDetail />
          <CardMenuDetail />
          <CardMenuDetail />
        </div>

        <div className="border rounded-base border-neutral-300 p-3 h-[224px]">
          <label className="text-label-medium text-neutral-900">
            Tagihan Pemesanan
          </label>

          <div className="flex flex-col gap-3 mt-2">
            <FieldColumn
              label="Biaya untuk Resto"
              value="Rp 4.000"
              size="medium"
              between
            />
            <FieldColumn
              label="Biaya Jasa Aplikasi"
              value="Rp 4.000"
              size="medium"
              between
            />
            <FieldColumn
              label="Subtotal Menu"
              value="Rp 4.000"
              size="medium"
              between
            />
            <FieldColumn
              label="Biaya Layanan"
              value="Rp 4.000"
              size="medium"
              between
            />
            <FieldColumn
              label="Subtotal Reservasi"
              value="Rp 4.000"
              size="medium"
              between
            />
          </div>
        </div>
      </div>

      <div className="flex gap-3 mt-8">
        <Button className="flex-1 bg-error-500 text-white !text-button-large">
          Tidak Datang
        </Button>
        <Button
          className="flex-1 text-neutral-900 !text-button-large"
          onClick={handleClickConfirmation}
        >
          Konfirmasi Kedatangan
        </Button>
      </div>
    </div>
  );
};

export default ModalDetailReservation;
