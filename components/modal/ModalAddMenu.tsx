import Image from 'next/image';
import CInput from '../shared/CInput';
import CountTotal from '../shared/CountTotal';

const ModalAddMenu = () => {
  return (
    <div className="grid grid-cols-5 gap-6">
      {/* left */}
      <div className="col-span-2 flex flex-col justify-between h-[350px]">
        <div className="relative h-[187px] rounded-base overflow-hidden">
          <Image
            src={'/images/photos/thumbnail.png'}
            alt="thumnail"
            style={{ objectFit: 'cover' }}
            fill
            priority
          />
        </div>
        <div>
          <div className="mb-5">
            <label className="text-body-xsmall text-neutral-700">Total</label>
            <h4 className="text-heading-4 text-secondary">Rp 55.000</h4>
          </div>

          <CountTotal />
        </div>
      </div>

      {/* right */}
      <div className="col-span-3 overflow-y-auto h-[370px] pb-10">
        <h4 className="text-heading-4 mb-2">Iga Bakar Special Rendang Jawa</h4>
        <div className="flex items-center gap-2 mb-4">
          <h2 className="text-body-xlarge text-secondary">Rp 45.000</h2>
          <span className="text-sm text-neutral-600 line-through">
            Rp 50.000
          </span>
        </div>

        <div className="border-y-[1.5px] border-neutral-300 py-[13px]">
          <label className="text-label-xsmall text-neutral-900">
            Varian A <span className="text-secondary">*</span>
          </label>
          {Array.from({ length: 3 }).map((_, index) => (
            <div className="flex w-full justify-between items-center">
              <div className="flex-shrink-0 text-neutral-900">
                Nama Varian A1
              </div>
              <div>
                <CInput className="w-6 h-6" type="radio" />
              </div>
            </div>
          ))}
        </div>
        <div className="border-y-[1.5px] border-neutral-300 py-[13px]">
          <label className="text-label-xsmall text-neutral-900">
            Varian A <span className="text-secondary">*</span>
          </label>
          {Array.from({ length: 4 }).map((_, index) => (
            <div className="flex w-full justify-between items-center">
              <div className="flex-shrink-0 text-neutral-900">
                Nama Varian A1
              </div>
              <div>
                <CInput className="w-6 h-6" type="checkbox" />
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ModalAddMenu;
