import React from 'react';
import FieldInformation from '../dashboard-list-order/FieldInformation';
import Divider from '../shared/Divider';
import CardMenuDetail from '../cards/CardMenuDetail';
import ErrorContent from '../shared/ErrorContent';
import { cn } from '@/lib/utils';

const ModalCancelOrder = () => {
  const status = 'Sudah Bayar';

  return (
    <>
      <div
        className={cn(
          'flex pb-6',
          status === 'Sudah Bayar' ? 'gap-10' : 'gap-5',
        )}
      >
        <FieldInformation label="No. Pesanan" value="ORD-20240403-49790" />
        <FieldInformation label="Waktu Pemesanan" value="16 Mei 2023 - 10:15" />
        <FieldInformation label="Tipe Pesanan" value="Dine In" />
        <FieldInformation label="Meja" value="Meja A01, Meja A02" />
      </div>
      <Divider color="border-neutral-300 mb-5" />

      {status === 'Sudah Bayar' && <ErrorContent />}

      {status !== 'Sudah Bayar' && (
        <div className="h-[500px] overflow-y-auto pb-5">
          {Array.from({ length: 15 }).map((_, index) => (
            <CardMenuDetail type="order" radio countButton />
          ))}
        </div>
      )}
    </>
  );
};

export default ModalCancelOrder;
