import ButtonBottom from '../button/ButtonBottom';
import CardMenuDetail from '../cards/CardMenuDetail';
import HeadingPrice from '../dashboard-payment/HeadingPrice';
import CInput from '../shared/CInput';
import Divider from '../shared/Divider';
import { Button } from '../ui/button';

const ModalSplitBill = () => {
  return (
    <div className="relative">
      <div className="mb-6">
        <HeadingPrice size="small" />
      </div>

      <div className="h-[613px] overflow-y-auto pb-52">
        <h6 className="text-heading-7 mb-5">Pisah Jumlah</h6>
        <CInput placeholder="Masukkan Nominal" borderBottom />

        <h6 className="text-heading-7 mt-6 mb-3">Pisah Jumlah</h6>
        {Array.from({ length: 10 }).map((_, index) => (
          <CardMenuDetail type="order" radio countButton />
        ))}
      </div>

      <ButtonBottom className="!absolute !bottom-0 !h-[185px] !p-0">
        <div className="w-full flex !flex-col py-5 h-full">
          <Divider className="border-2 mb-3" color="border-neutral-300" />

          <label className="text-body-small mb-1">Jumlah Pisah</label>
          <label className="text-label-large text-success-500 mb-6">Rp 0</label>
          <Button className="mb-1 !text-button-large">Bayar</Button>
        </div>
      </ButtonBottom>
    </div>
  );
};

export default ModalSplitBill;
