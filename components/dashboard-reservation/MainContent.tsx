'use client';

import Ctable from '@/components/shared/Table';
import { Button } from '@/components/ui/button';
import { TableCell, TableRow } from '@/components/ui/table';

import { reservationTableBody, reservationTableHeader } from '@/constants';
import { setOpenModal } from '@/store/modal/action';
import { useDispatch } from 'react-redux';

const MainContent = () => {
  const dispatch = useDispatch();

  return (
    <div className="overflow-y-auto h-svh pb-52 mt-3">
      <Ctable headerData={reservationTableHeader}>
        {reservationTableBody.slice(0, 10).map((item, index) => (
          <TableRow className="!py-6" key={item.numberOrder}>
            <TableCell className="font-medium">{item.numberOrder}</TableCell>
            <TableCell className="font-medium">{item.date}</TableCell>
            <TableCell className="font-medium">{item.time}</TableCell>
            <TableCell className="font-medium">{item.name}</TableCell>
            <TableCell className="font-medium">{item.phone}</TableCell>
            <TableCell className="font-medium">{item.guestCount}</TableCell>
            <TableCell className="font-medium">{item.areaName}</TableCell>
            <TableCell className="font-medium">
              <Button
                className="bg-neutral-200 w-20 h-10 !text-button-medium"
                onClick={() =>
                  dispatch(
                    setOpenModal({
                      modalName: 'reservationDetailModal',
                      width: 'w-[776px]',
                      height: 'h-[699px]',
                      headerTitle: 'Detail Reservasi',
                      showFooter: false,
                    }),
                  )
                }
              >
                Detail
              </Button>
            </TableCell>
          </TableRow>
        ))}
      </Ctable>
    </div>
  );
};

export default MainContent;
