import { cn } from '@/lib/utils';
import DividerTicket from '../dashboard-payment/DividerTicket';

const LabelItem = ({
  label,
  value,
  className,
}: {
  label: string;
  value: string;
  className?: string;
}) => {
  return (
    <div className={cn('flex justify-between', className)}>
      <label className="text-body-medium text-neutral-900">{label}</label>
      <span className="text-label-large text-neutral-900">{value}</span>
    </div>
  );
};
const CardTicketReservation = () => {
  return (
    <div className="mt-6 bg-secondary-50 p-3 h-[194px] relative rounded-base">
      <div className="flex flex-col gap-[18px]">
        <LabelItem label="Metode Pembayaran" value="Cash" />
        <LabelItem label="Total" value="Rp 123.000" />
        <LabelItem label="Cash" value="Rp 123.000" />
      </div>

      <DividerTicket />

      <LabelItem label="Total" value="Rp 123.000" className="mt-4" />
    </div>
  );
};

export default CardTicketReservation;
