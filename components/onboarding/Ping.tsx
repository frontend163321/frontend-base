import { cn } from '@/lib/utils';
import React from 'react';

const Ping = ({ color }: { color: string | undefined }) => {
  return (
    <span className="relative flex h-3 w-3">
      <span
        className={cn(
          'animate-ping absolute inline-flex h-full w-full rounded-full opacity-75',
          color,
        )}
      ></span>
      <span
        className={cn('relative inline-flex rounded-full h-3 w-3', color)}
      ></span>
    </span>
  );
};

export default Ping;
