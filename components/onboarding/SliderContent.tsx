'use client';

import {
  Carousel,
  CarouselApi,
  CarouselContent,
} from '@/components/ui/carousel';
import { ReactNode, useEffect } from 'react';

type SliderContentProps = {
  api: CarouselApi;
  setApi: (api: CarouselApi) => void;
  children: ReactNode;
  setCount: (value: number) => void;
  setCurrent: (value: number) => void;
};

const SliderContent = ({
  api,
  setApi,
  children,
  setCurrent,
  setCount,
}: SliderContentProps) => {
  useEffect(() => {
    if (!api) {
      return;
    }

    setCount(api.scrollSnapList().length);
    setCurrent(api.selectedScrollSnap() + 1);

    api.on('select', () => {
      setCurrent(api.selectedScrollSnap() + 1);
    });
  }, [api]);

  return (
    <Carousel setApi={setApi}>
      <CarouselContent className="w-full">{children}</CarouselContent>
    </Carousel>
  );
};

export default SliderContent;
