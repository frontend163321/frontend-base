import React from 'react';
import styles from '@styles/modules/onboarding.module.css';

type DotsIndicatorProps = {
  count: number;
  current: number;
};

const DotsIndicator = ({ count, current }: DotsIndicatorProps) => {
  return (
    <div className={`${styles.dots} mt-[16px]`}>
      {Array.from({ length: count }).map((_, index) => (
        <div
          className={`h-3 w-3 rounded-full ${
            index + 1 === current
              ? 'h-3 w-8 bg-primary'
              : index + 1 < current
                ? 'bg-muted-foreground'
                : 'bg-muted'
          }`}
          key={index}
        />
      ))}
    </div>
  );
};

export default DotsIndicator;
