import { cn } from '@/lib/utils';
import { Button } from '../ui/button';
import Ping from './Ping';

const TableStatus = ({ label, active, setActive }: TableStatus) => {
  const borderColor: any = {
    Semua: `border-info-500`,
    Available: `border-success-500`,
    Filled: `border-neutral-500`,
    'Available Soon': `border-warning-500`,
  };

  const pingColor: any = {
    Semua: `bg-info-500`,
    Available: `bg-success-500`,
    Reserved: 'bg-error-500',
    Filled: `bg-neutral-500`,
    'Available Soon': `bg-warning-500`,
  };

  return (
    <Button
      className={cn(
        `text-label-small flex gap-[6px] px-4 h-[36px] rounded-full border`,
        active === label
          ? `bg-transparent ${borderColor[label]}`
          : '!bg-neutral-200 !border-neutral-200',
      )}
      variant={'ghost'}
      onClick={() => setActive(label)}
    >
      <Ping color={pingColor[label]} />
      {label}
      <span>(60)</span>
    </Button>
  );
};

export default TableStatus;
