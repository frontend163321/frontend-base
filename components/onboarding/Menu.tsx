'use client';

import CardMenu from '@/components/cards/CardMenu';
import { setOpenModal } from '@/store/modal/action';
import styles from '@styles/modules/dashboard.module.css';
import { useDispatch } from 'react-redux';

const Menu = () => {
  const dispatch = useDispatch();
  return (
    <div className={styles.menu}>
      <div className={styles.menuGrid}>
        {Array.from({ length: 30 }).map((_, index) => (
          <CardMenu
            label
            stars
            onClick={() =>
              dispatch(
                setOpenModal({
                  headerTitle: 'Tambah Produk',
                  footerTitle: 'Tambah',
                  modalName: 'ModalAddMenu',
                  height: 'h-[500px]',
                  width: 'w-[616px]',
                }),
              )
            }
            key={index}
          />
        ))}
      </div>
    </div>
  );
};

export default Menu;
