import { cn } from '@/lib/utils';
import React from 'react';

const LabelOrderStatus = ({ value }: { value: string }) => {
  const labelColor = () => {
    switch (value) {
      case 'Belum Bayar':
        return 'border-secondary-500 text-secondary-500';
      case 'Sebagian Bayar':
        return 'border-warning-500 text-warning-500';
      case 'Sudah Bayar':
        return 'border-success-500 text-success-500';
    }
  };

  return (
    <div
      className={cn(
        'px-[6px] py-[2px] border w-fit !text-label-xsmall rounded-[2px]',
        labelColor(),
      )}
    >
      {value}
    </div>
  );
};

export default LabelOrderStatus;
