'use client';

import {
  BlackCheckCircleIcon,
  CancelOrderIcon,
  SplitBillIcon,
} from '@/lib/images';
import Image from 'next/image';
import ButtonBottom from '../button/ButtonBottom';
import { Button } from '../ui/button';
import { useDispatch } from 'react-redux';
import { setOpenModal } from '@/store/modal/action';

const BottomSheet = () => {
  const dispatch = useDispatch();
  const status = 'Sudah Bayar';

  return (
    <ButtonBottom className="!absolute !bottom-[4.8rem]" shadow>
      <div className="flex w-full justify-between">
        <Button
          variant={'outline'}
          className="border-secondary-500 !text-button-large w-[176px] text-secondary-500 gap-2"
          onClick={() =>
            dispatch(
              setOpenModal({
                modalName: 'cancelOrderModal',
                width: status === 'Sudah Bayar' ? 'w-[708px]' : 'w-[811px]',
                height: 'h-[700px]',
                headerTitle: 'Batal Pesanan',
                footerTitle:
                  status === 'Sudah Bayar' ? 'Kembali' : 'Batalkan Pesanan',
                shadowFooter: false,
              }),
            )
          }
        >
          <Image src={CancelOrderIcon} alt="cancel order icon" />
          Batal Pesanan
        </Button>

        <div className="flex gap-3">
          <Button
            variant={'outline'}
            className="border-primary !text-button-large w-[180px] text-neutral-900 gap-2"
            onClick={() =>
              dispatch(
                setOpenModal({
                  modalName: 'splitBillModal',
                  width: 'w-[811px]',
                  height: 'h-[746px]',
                  headerTitle: 'Pisah Tagihan',
                  showFooter: false,
                }),
              )
            }
          >
            <Image src={SplitBillIcon} alt="cancel order icon" />
            Bagi Tagihan
          </Button>
          <Button
            variant={'default'}
            className="border-secondary-500 !text-button-large w-[180px] text-neutral-900 gap-2"
          >
            <Image src={BlackCheckCircleIcon} alt="cancel order icon" />
            Selesaikan
          </Button>
        </div>
      </div>
    </ButtonBottom>
  );
};

export default BottomSheet;
