import React from 'react';
import FieldDotColumn from '../shared/FieldDotColumn';
import LabelOrderStatus from './LabelOrderStatus';
import { cn } from '@/lib/utils';

const CardOrder = ({
  data,
  active,
  setActive,
}: {
  data: any;
  active: string;
  setActive: (active: string) => void;
}) => {
  const borderColor = () => {
    switch (data?.status) {
      case 'Belum Bayar':
        return 'border-secondary-500';
      case 'Sebagian Bayar':
        return 'border-warning-500';
      case 'Sudah Bayar':
        return 'border-success-500';
    }
  };

  return (
    <div
      className={cn(
        'p-3 border border-neutral-300 rounded-base',
        active === data.orderId && borderColor(),
      )}
      onClick={() => setActive(data.orderId)}
    >
      <div className="flex justify-between mb-[1px]">
        <label className="text-body-xsmall text-neutral-600">{data.name}</label>

        <FieldDotColumn
          label="Dine In"
          value="10:15"
          fontSize="text-body-xsmall"
          color="text-neutral-600"
        />
      </div>

      <div className="flex gap-5 justify-between mb-[6px]">
        <LabelOrderStatus value={data?.status} />
        <span className="text-label-small text-neutral-600">{data.table}</span>
      </div>

      <div className="flex gap-5 justify-between">
        <label className="text-label-medium text-neutral-900">
          {data?.orderId}
        </label>
        <span className="text-label-medium text-neutral-900">
          {data?.price}
        </span>
      </div>
    </div>
  );
};

export default CardOrder;
