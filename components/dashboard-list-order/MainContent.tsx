import CardMenuDetail from '@components/cards/CardMenuDetail';
import LayoutMain from '@components/layout/LayoutMain';
import BottomSheet from './BottomSheet';
import FieldInformation from './FieldInformation';

const MainContent = () => {
  return (
    <>
      <div className="p-5 bg-secondary-50">
        <div className="flex gap-5">
          <FieldInformation label="No. Pesanan" value="ORD-20240403-49790" />
          <FieldInformation
            label="Waktu Pemesanan"
            value="16 Mei 2023 - 10:15"
          />
          <FieldInformation label="Tipe Pesanan" value="Dine In" />
          <FieldInformation label="Meja" value="Meja A01, Meja A02" />
          <FieldInformation label="Status" value="Belum Bayar" />
        </div>
      </div>

      <LayoutMain className="h-svh overflow-y-auto pb-64">
        {Array.from({ length: 15 }).map((_, index) => (
          <CardMenuDetail type="order" countButton />
        ))}
      </LayoutMain>

      <BottomSheet />
    </>
  );
};

export default MainContent;
