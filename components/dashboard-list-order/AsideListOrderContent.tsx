'use client';

import { listOrderFilter, listTransaction } from '@/constants';
import { SearchIcon } from '@/lib/images';
import LayoutMain from '../layout/LayoutMain';
import CInput from '../shared/CInput';
import CSelect from '../shared/CSelect';
import CardOrder from './CardOrder';
import { useState } from 'react';

const AsideListOrderContent = () => {
  const [active, setActive] = useState<string>('');

  return (
    <LayoutMain>
      <div className="flex gap-2">
        <CInput
          icon={SearchIcon}
          iconSize={20}
          placeholder="Cari Pesanan"
          size="medium"
          classNameParent="!w-[257px]"
        />
        <CSelect
          data={listOrderFilter}
          placeholder={listOrderFilter[0].label}
          size="medium"
          className="!w-[257px]"
          icon={false}
        />
      </div>

      <div className="mt-5">
        <h6 className="text-heading-7 text-neutral-900 mb-2">11 Transaksi</h6>
        <div className="flex flex-col gap-2 overflow-y-auto h-screen pb-52">
          {listTransaction.map((item, index) => (
            <CardOrder data={item} active={active} setActive={setActive} />
          ))}
        </div>
      </div>
    </LayoutMain>
  );
};

export default AsideListOrderContent;
