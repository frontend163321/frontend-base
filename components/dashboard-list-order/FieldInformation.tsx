import { cn } from '@/lib/utils';
import React from 'react';

const FieldInformation = ({
  label,
  value,
}: {
  label: string;
  value: string;
}) => {
  const labelColor = () => {
    switch (value) {
      case 'Belum Bayar':
        return 'text-secondary-500';
      case 'Sebagian Bayar':
        return 'text-warning-500';
      case 'Sudah Bayar':
        return 'text-success-500';
    }
  };
  return (
    <div className="flex-shrink-0">
      <p className="text-body-xsmall text-neutral-600 mb-[6px]">{label}</p>
      <label
        className={cn('!text-label-medium text-neutral-900', labelColor())}
      >
        {value}
      </label>
    </div>
  );
};

export default FieldInformation;
