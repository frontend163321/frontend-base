import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

//* reducer
import modal from './modal/reducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: [],
};

const rootReducer = combineReducers({
  modal,
});

const persistedReducer = persistReducer(persistConfig, rootReducer as any);

const isProduction = process.env.NODE_ENV === 'production';

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({ serializableCheck: false }),
  devTools: isProduction ? false : true,
});

export default store;

export type IRootState = ReturnType<typeof rootReducer>;
