import { SET_CLOSE_MODAL, SET_OPEN_MODAL } from './constants';

type Modal = {
  headerTitle?: string;
  footerTitle?: string;
  data?: any;
  modalName: string;
  height: string;
  width: string;
  shadowFooter?: boolean;
  showFooter?: boolean;
  showHeader?: boolean;
  paddingContent?: boolean;
};

export const setOpenModal = ({
  data,
  height,
  width,
  modalName,
  headerTitle,
  footerTitle,
  shadowFooter,
  showFooter,
  showHeader,
  paddingContent,
}: Modal) => {
  return {
    type: SET_OPEN_MODAL,
    headerTitle,
    footerTitle,
    modalName,
    shadowFooter,
    data,
    height,
    width,
    showFooter,
    showHeader,
    paddingContent,
  };
};

export const setCloseModal = () => {
  return {
    type: SET_CLOSE_MODAL,
  };
};
