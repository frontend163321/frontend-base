import { SET_CLOSE_MODAL, SET_OPEN_MODAL } from './constants';

const initialState = {
  open: false,
  shadowFooter: true,
  showFooter: true,
  showHeader: true,
  paddingContent: true,
  headerTitle: '',
  footerTitle: '',
  modalName: '',
  data: null,
  width: '',
  height: '',
};

export default function reducer(state = initialState, action: any) {
  switch (action.type) {
    case SET_OPEN_MODAL:
      return {
        open: true,
        headerTitle: action.headerTitle,
        footerTitle: action.footerTitle,
        shadowFooter: action.shadowFooter,
        showFooter: action.showFooter,
        showHeader: action.showHeader,
        paddingContent: action.paddingContent,
        modalName: action.modalName,
        data: action.data,
        height: action.height,
        width: action.width,
      };

    case SET_CLOSE_MODAL:
      return {
        ...initialState,
      };

    default:
      return state;
  }
}
